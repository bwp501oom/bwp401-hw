﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="sign_in.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>تسجيل الدّخول&nbsp;|&nbsp;EShop</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <div class="main inside-main">
                <div class="mid">
                    تسجيل الدخول
                </div>
                <div class="sign-in-form">
                    <div class="mid">
                        <img src="pics/Signin_icon.svg" alt="" style="width: 47.28px; height: 50.66px;" />
                    </div>
                    <div class="mid-form">
                        <asp:Label runat="server" ID="EmailLabel">البريد الإلكترونيّ</asp:Label>
                        <br />
                        <asp:TextBox runat="server" ID="EmailTB" CssClass="input"></asp:TextBox>
                        <br />
                        <asp:Label runat="server" ID="PasswordLabel">كلمة السر</asp:Label>
                        <br />
                        <asp:TextBox runat="server" ID="PasswordTB" CssClass="input" TextMode="Password"></asp:TextBox>
                        <br />
                        <asp:Button runat="server" ID="LoginBtn" CssClass="submit-btn2" Text="دخول" OnClick="LoginBtn_Click"/>
                        <br />
                        <asp:Label runat="server" ID="LoginError" Text="خطأ في تسجيل الدخول، تأكد من معلومات الحساب" Visible="false"></asp:Label>
                    </div>
                </div>
            </div>
</asp:Content>

