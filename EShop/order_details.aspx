﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="order_details.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>معلومات الطلب&nbsp;|&nbsp;EShop</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <style type="text/css">
    .Grid, .Grid th, .Grid td
    {
        border:none;
    }
    #removethis {
        color:white;
    }
    .noselect {
  -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
     -khtml-user-select: none; /* Konqueror HTML */
       -moz-user-select: none; /* Old versions of Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome, Edge, Opera and Firefox */
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <asp:Panel runat="server" ID="asd" CssClass="main inside-main" Direction="RightToLeft">
        <br />
        <br />
        <div class="row text-center">
            <div class="col-md-12">
                <asp:Button runat="server" ID="PrintBtn" Text="طباعة" CssClass="submit-btn-edited" OnClick="PrintBtn_Click" />
            </div>
        </div>
        <br />
        <div class="row text-center" style="margin: auto; background-color: #232F3E; color: white; font-size: 24px;" runat="server" id="TopHeader">
            <div class="col-md-5">
                <asp:Label runat="server" Text="صورة المنتج"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="اسم المنتج"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="الكمية"></asp:Label>
            </div>
            <div class="col-md-3">
                <asp:Label runat="server" Text="السعر الاجمالي"></asp:Label>
            </div>
        </div>
        <asp:Repeater runat="server" ID="Repeater1">
            <ItemTemplate>
                <div class="row  text-center" style="margin: auto;">
                    <div class="col-md-5">
                        <img class="text-right" src='<%#Eval("product_image")%>' alt="Card image cap" style="width: 40%;">
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("product_name")%>'></asp:Label>
                        <asp:Label runat="server" Text='<%#Eval("Id")%>' Visible="false" ID="ProductId"></asp:Label>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("quantity")%>'></asp:Label>
                    </div>
                    <div class="col-md-3" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("total_price")%>'></asp:Label>
                    </div>
                </div>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <div class="row  text-center" style="margin: auto; background-color: #EFF0F1;">
                    <div class="col-md-5">
                        <img class="text-right" src='<%#Eval("product_image")%>' alt="Card image cap" style="width: 40%;">
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("product_name")%>'></asp:Label>
                        <asp:Label runat="server" Text='<%#Eval("Id")%>' Visible="false" ID="ProductId"></asp:Label>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("quantity")%>'></asp:Label>
                    </div>
                    <div class="col-md-3" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("total_price")%>'></asp:Label>
                    </div>
                </div>
            </AlternatingItemTemplate>
        </asp:Repeater>

        <br />
        <br />
        <br />

        <div style="color:white; border-color:white;"  class="noselect" >
            <asp:Panel runat="server" ID="printPanel" Direction="RightToLeft" CssClass="text-center"  style="display:none;">
                <p  style="font-size:50px;">EShop</p>
                <br /><br /><br />
                <p style="font-size:40px;">Order Details</p>
                <br />
                <asp:GridView runat="server" ID="gridviewData" CssClass="Grid">
                    <HeaderStyle/>
                </asp:GridView>
                <p>Total Price in SYP:</p>
                <asp:Label runat="server" ID="total" Text=""></asp:Label>
                <br />
            </asp:Panel>
        </div>
    </asp:Panel>
</asp:Content>