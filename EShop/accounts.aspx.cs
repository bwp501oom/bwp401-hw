﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] != null)
        {
            if (!this.IsPostBack)
            {
                using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
                {
                    string query = "SELECT is_admin from Accounts where username = @username";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@username", Session["username"].ToString());
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string admin = ds.Tables[0].Rows[0]["is_admin"].ToString();
                        if (admin == "True")
                        {
                            BindRepeator();
                        }
                        else
                        {
                            Response.Redirect("index.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("index.aspx");
                    }

                }
            }
        }
        else
        {
            Response.Redirect("index.aspx");
        }
    }
    private void BindRepeator()
    {
        using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
        {
            string query = "SELECT Id, username, email, password, address, mobile_number, is_admin FROM Accounts";
            SqlCommand cmd = new SqlCommand(query, con);
            DataTable dt = new DataTable();
            con.Open();
            dt.Load(cmd.ExecuteReader());
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
        }
    }
}