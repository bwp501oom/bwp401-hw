﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Text.RegularExpressions;
using System.IO;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] != null)
        {
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
            {

                string query = "SELECT is_admin from Accounts WHERE username = @username";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@username", Session["username"].ToString());
                con.Open();
                string admin = Convert.ToString(cmd.ExecuteScalar());
                if (admin == "True")
                {
                    DeleteProductDiv.Visible = true;
                    ProductOrderDiv.Visible = true;
                }
                else
                {
                    ProductOrderDiv.Visible = true;
                }
                con.Close();
            }
            
        }

        if (!this.IsPostBack)
        {
            if (UpdateProductDiv.Visible == true)
            {
                ProductOrderDiv.Visible = false;
            }
            lblId.Text = HttpUtility.UrlDecode(Request.QueryString["Id"]);
            try
            {
                using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
                {
                    con.Open();
                    string query = "SELECT * from Products where id = @id";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@id", lblId.Text);
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        productName.Text += ds.Tables[0].Rows[0]["product_name"].ToString();
                        string PT = ds.Tables[0].Rows[0]["product_type"].ToString();
                        HiddenProductType.Text = PT;
                        if (PT == "pc")
                        {
                            productType.Text = "حاسب";
                        }
                        else if (PT == "laptop")
                        {
                            productType.Text = "حاسب محمول";
                        }
                        else if (PT == "printer")
                        {
                            productType.Text = "طابعة";
                        }
                        else if (PT == "scanner")
                        {
                            productType.Text = "ماسح ضوئي";
                        }
                        productColor.Text += ds.Tables[0].Rows[0]["color"].ToString();
                        productBrand.Text += ds.Tables[0].Rows[0]["product_brand"].ToString();
                        productYear.Text += ds.Tables[0].Rows[0]["product_year"].ToString();
                        productPrice.Text += ds.Tables[0].Rows[0]["price"].ToString();
                        productImage.Src = ds.Tables[0].Rows[0]["image"].ToString();
                        productStock.Text = ds.Tables[0].Rows[0]["stock"].ToString();
                        imagePath.Text = ds.Tables[0].Rows[0]["image"].ToString();
                        product_id.Text = ds.Tables[0].Rows[0]["Id"].ToString();
                        product_image.Text = ds.Tables[0].Rows[0]["image"].ToString();
                        product_name.Text = ds.Tables[0].Rows[0]["product_name"].ToString();

                        pcStorage.Text += ds.Tables[0].Rows[0]["pc_storage"].ToString();
                        pcRam.Text += ds.Tables[0].Rows[0]["pc_ram"].ToString();
                        pcProcessor.Text += ds.Tables[0].Rows[0]["pc_processor"].ToString();
                        pcModel.Text += ds.Tables[0].Rows[0]["pc_model"].ToString();
                        pcGpu.Text += ds.Tables[0].Rows[0]["pc_gpu"].ToString();
                        pcResolution.Text += ds.Tables[0].Rows[0]["pc_resolution"].ToString();

                        printerResolution.Text += ds.Tables[0].Rows[0]["printer_resolution"].ToString();
                        printerSpeed.Text += ds.Tables[0].Rows[0]["printer_speed"].ToString();
                        printerPaper.Text += ds.Tables[0].Rows[0]["printer_paper"].ToString();
                        string PRT = ds.Tables[0].Rows[0]["printer_type"].ToString();
                        if (PRT == "scanner")
                        {
                            printerType.Text = "كبيرة مع ماسح ضوئي";
                        }
                        else if (PRT == "printer")
                        {
                            printerType.Text = "صغيرة مكتبية";
                        }
                        printerMemory.Text += ds.Tables[0].Rows[0]["printer_memory"].ToString();

                        string ST = ds.Tables[0].Rows[0]["scanner_type"].ToString();
                        if (ST == "flat")
                        {
                            scannerType.Text = "مسطح";
                        }
                        else if (ST == "sheet")
                        {
                            scannerType.Text = "سطحي";
                        }
                        else if (ST == "convenience")
                        {
                            scannerType.Text = "شرائحي";
                        }
                        else if (ST == "multi")
                        {
                            scannerType.Text = "متعدد الوظائف";
                        }
                        scannerSpeed.Text += ds.Tables[0].Rows[0]["scanner_speed"].ToString();
                        scannerPaper.Text += ds.Tables[0].Rows[0]["scanner_paper"].ToString();
                        scannerResolution.Text += ds.Tables[0].Rows[0]["scanner_resolution"].ToString();

                        string type = ds.Tables[0].Rows[0]["product_type"].ToString();
                        if (type == "pc" || type == "laptop")
                        {
                            pcDiv.Visible = true;
                        }
                        else if (type == "printer")
                        {
                            printerDiv.Visible = true;
                            string type2 = ds.Tables[0].Rows[0]["printer_type"].ToString();
                            if (type2 == "scanner"){
                                printerMemoryDiv.Visible = true;
                            }

                        }
                        else if (type == "scanner")
                        {
                            scannerDiv.Visible = true;
                        }
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {

            }
        }
        if (UpdateProductDiv.Visible == true)
        {
            ProductOrderDiv.Visible = false;
        }
        if (Session["username"] != null)
        {
            if (Convert.ToInt32(productStock.Text) == 0)
            {
                OutOfStockDiv.Visible = true;
                InStockDiv.Visible = false;
                ProductOrderDiv.Visible = false;
            }
            else
            {
                OutOfStockDiv.Visible = false;
                InStockDiv.Visible = true;
                ProductOrderDiv.Visible = true;
            }
        }
        
    }
    protected void QuantityPlus_Click(object sender, EventArgs e)
    {
        ViewState["count"] = Convert.ToInt32(ViewState["count"]) + 1;
        int count = Convert.ToInt32(ViewState["count"]);
        QuantityTB.Text = count.ToString();
        int price = Convert.ToInt32(Regex.Match(productPrice.Text, @"\d+").Value);
        price = price * count;
        TotalPriceLabel.Text = price.ToString() + " ليرة سورية ";
    }
    protected void QuantityMinus_Click(object sender, EventArgs e)
    {
        
        if (Convert.ToInt32(ViewState["count"]) < 2)
        {

        }
        else
        {
            ViewState["count"] = Convert.ToInt32(ViewState["count"]) - 1;
            int count = Convert.ToInt32(ViewState["count"]);
            QuantityTB.Text = count.ToString();
            int price = Convert.ToInt32(Regex.Match(productPrice.Text, @"\d+").Value);
            price = price * count;
            TotalPriceLabel.Text = price.ToString() + " ليرة سورية ";
        }
        
    }
    protected void SubmitItemOrder_Click(object sender, EventArgs e)
    {
        string productId = HttpUtility.UrlDecode(Request.QueryString["Id"]);
        int currentStock = Convert.ToInt32(productStock.Text);
        int orderCount = Convert.ToInt32(ViewState["count"]);
        if (TotalPriceLabel.Text.Length > 0)
        {
            if (Convert.ToInt32(QuantityTB.Text) <= Convert.ToInt32(productStock.Text))
            {
                using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
                {
                    string query = "INSERT INTO Order_Item (username_id, product_id, quantity, total_price, product_image, product_name) VALUES (@username_id, @product_id, @quantity, @total_price, @product_image, @product_name)";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@username_id", Session["username"].ToString());
                    cmd.Parameters.AddWithValue("@product_id", product_id.Text);
                    cmd.Parameters.AddWithValue("@quantity", ViewState["count"].ToString());
                    cmd.Parameters.AddWithValue("@total_price", TotalPriceLabel.Text);
                    cmd.Parameters.AddWithValue("@product_image", product_image.Text);
                    cmd.Parameters.AddWithValue("@product_name", product_name.Text);
                    con.Open();
                    int i = cmd.ExecuteNonQuery();
                    if (i != 0)
                    {
                        validatorr.Text = "تم اضافة المنتج الى سلة الشراء";
                        ViewState["count"] = 0;
                        TotalPriceLabel.Text = "";
                        QuantityTB.Text = "";
                        string query2 = "UPDATE Products SET stock = @stock WHERE Id = @Id";
                        SqlCommand cmd2 = new SqlCommand(query2, con);
                        int newStock = currentStock - orderCount; 
                        cmd2.Parameters.AddWithValue("@Id", productId);
                        cmd2.Parameters.AddWithValue("@stock", newStock);
                        SqlDataAdapter sda = new SqlDataAdapter();
                        sda.UpdateCommand = cmd2;
                        sda.UpdateCommand.ExecuteNonQuery();
                        Response.Redirect(Request.RawUrl);
                        
                    }
                    con.Close();
                 }
            }
            else
            {
                validatorr.Text = "الكمية المطلوبة اكبر من الكمية التي بالمستودع";
            }
        }
        else
        {
            validatorr.Text = "الرجاء التأكد من ادخال الكمية";
        }
    }
    protected void DeleteProduct_Click(object sender, EventArgs e)
    {
        string productId = HttpUtility.UrlDecode(Request.QueryString["Id"]);
        using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
        {
            string query = "DELETE FROM Products WHERE Id = @Id";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@Id", productId);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            Response.Redirect("products.aspx");
            
        }
    }
    protected void EditProduct_Click(object sender, EventArgs e)
    {
        TextboxClassChangeCall(2);
        ProductOrderDiv.Visible = false;
        EditImageDiv.Visible = true;
        ExtraDetailsDiv.Visible = false;
        UpdateProductDiv.Visible = true;
        InStockDiv.Visible = true;
    }
    protected void TextboxClassChange(TextBox textboxName, int num)
    {
        if (num == 1)
        {
            textboxName.CssClass = "label-box";
            textboxName.ReadOnly = true;
        }
        else
        {
            textboxName.CssClass = "box-edited";
            textboxName.ReadOnly = false;
        }

    }
    protected void TextboxClassChangeCall(int num)
    {
        TextboxClassChange(productName, num);
        TextboxClassChange(productColor, num);
        TextboxClassChange(productBrand, num);
        TextboxClassChange(productYear, num);
        TextboxClassChange(productPrice, num);
        TextboxClassChange(pcStorage, num);
        TextboxClassChange(pcRam, num);
        TextboxClassChange(pcProcessor, num);
        TextboxClassChange(pcGpu, num);
        TextboxClassChange(pcResolution, num);
        TextboxClassChange(printerResolution, num);
        TextboxClassChange(printerSpeed, num);
        TextboxClassChange(printerPaper, num);
        TextboxClassChange(printerMemory, num);
        TextboxClassChange(scannerSpeed, num);
        TextboxClassChange(scannerPaper, num);
        TextboxClassChange(scannerResolution, num);
        TextboxClassChange(pcModel, num);
        TextboxClassChange(productStock, num);

    }
    protected void UpdateProduct_Click(object sender, EventArgs e)
    {
        bool stock = IsDigitsOnly(productStock.Text);
        bool year = IsDigitsOnly(productYear.Text);
        bool price = IsDigitsOnly(productPrice.Text);
        if (productName.Text.Length <= 0 || productColor.Text.Length <= 0 || productBrand.Text.Length <= 0 || productYear.Text.Length <= 0 || productPrice.Text.Length <= 0 || productStock.Text.Length <= 0)
        {
            validatorr.Text = "الرجاء التأكد من ادخال المعلومات الاساسية.";
        }
        else
        {
            if (stock == true && year == true && price == true)
            {
                using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
                {
                    string query = "UPDATE Products SET product_name = @product_name, product_type = @product_type, product_brand = @product_brand, product_year = @product_year, pc_storage = @pc_storage, pc_ram = @pc_ram, pc_processor = @pc_processor, pc_resolution = @pc_resolution, pc_model = @pc_model, pc_gpu = @pc_gpu, printer_resolution = @printer_resolution, printer_speed = @printer_speed, printer_paper = @printer_paper, printer_memory = @printer_memory, scanner_speed = @scanner_speed, scanner_paper = @scanner_paper, scanner_resolution = @scanner_resolution, price = @price, color = @color, image = @image, stock = @stock WHERE Id = @Id";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@product_name", productName.Text);
                    cmd.Parameters.AddWithValue("@product_type", HiddenProductType.Text);
                    cmd.Parameters.AddWithValue("@product_brand", productBrand.Text);
                    cmd.Parameters.AddWithValue("@color", productColor.Text);
                    cmd.Parameters.AddWithValue("@product_year", productYear.Text);
                    cmd.Parameters.AddWithValue("@price", productPrice.Text);
                    cmd.Parameters.AddWithValue("@image", imagePath.Text);
                    cmd.Parameters.AddWithValue("@pc_storage", pcStorage.Text);
                    cmd.Parameters.AddWithValue("@pc_ram", pcRam.Text);
                    cmd.Parameters.AddWithValue("@pc_processor", pcProcessor.Text);
                    cmd.Parameters.AddWithValue("@pc_gpu", pcGpu.Text);
                    cmd.Parameters.AddWithValue("@pc_resolution", pcResolution.Text);
                    cmd.Parameters.AddWithValue("@pc_model", pcModel.Text);
                    cmd.Parameters.AddWithValue("@printer_resolution", printerResolution.Text);
                    cmd.Parameters.AddWithValue("@printer_speed", printerSpeed.Text);
                    cmd.Parameters.AddWithValue("@printer_paper", printerPaper.Text);
                    cmd.Parameters.AddWithValue("@printer_memory", printerMemory.Text);
                    cmd.Parameters.AddWithValue("@scanner_speed", scannerSpeed.Text);
                    cmd.Parameters.AddWithValue("@scanner_paper", scannerPaper.Text);
                    cmd.Parameters.AddWithValue("@scanner_resolution", scannerResolution.Text);
                    cmd.Parameters.AddWithValue("@stock", productStock.Text);
                    cmd.Parameters.AddWithValue("@Id", lblId.Text);
                    con.Open();
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.UpdateCommand = cmd;
                    sda.UpdateCommand.ExecuteNonQuery();
                    validatorr.Text = "تم تعديل معلومات المنتج";
                    con.Close();
                    ProductOrderDiv.Visible = true;
                    UpdateProductDiv.Visible = false;
                    Response.Redirect(Request.RawUrl);
                }
                TextboxClassChangeCall(1);
                ProductOrderDiv.Visible = true;
                EditImageDiv.Visible = false;
                ExtraDetailsDiv.Visible = true;
            }
            else
            {
                validatorr.Text = "الرجاء التحقق من ادخال المعلومات الاساسية.";
            }
        }
        
    }
    protected void Unnamed_Click(object sender, EventArgs e)
    {

        string strFileName;
        string strFilePath;
        string strFolder;
        strFolder = Server.MapPath("~/pics/ProductImages/");
        strFileName = oFile.PostedFile.FileName;
        strFileName = Path.GetFileName(strFileName);
        strFilePath = strFolder + strFileName;
        oFile.PostedFile.SaveAs(strFilePath);
        imagePath.Text = "pics/ProductImages/" + strFileName;
        productImage.Src = imagePath.Text;
    }
    bool IsDigitsOnly(string str)
    {
        foreach (char c in str)
        {
            if (c < '0' || c > '9')
                return false;
        }

        return true;
    }
}