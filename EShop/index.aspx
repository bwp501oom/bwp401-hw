﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>الرئيسيّة&nbsp;|&nbsp;EShop</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <div class="main inside-main">
        <!-- First Product-->
        <div class="image-float-right">
            <img src="pics/products/Laptop_2.jpg" width="315" height="230" alt="Laptop EShop" />
        </div>
        <div class="h1-left">
            <h1>أحدث الأجهزة المحمولة<br />
                بمواصفات متعددة</h1>
        </div>
        <br />
        <!-- Second Product-->
        <div class="image-float-left">
            <img src="pics/products/printer_1.jpg" width="315" height="230" alt="Laptop EShop" />
        </div>
        <div class="h1-right">
            <h1>أحدث الطابعات<br />
                لمختلف الاحتياجات</h1>
        </div>
        <br />
        <!-- Third Product-->
        <div class="image-float-right">
            <img src="pics/products/Scanner.jpg" width="315" height="230" alt="Laptop EShop" />
        </div>
        <div class="h1-left">
            <h1>ماسحات ضوئيّة<br />
                بسرعات عالية</h1>
        </div>
        <br />
        <!-- Fourth Product-->
        <div class="image-float-left">
            <img src="pics/products/Computer_4.jpg" width="315" height="230" alt="Laptop EShop" />
        </div>
        <div class="h1-right">
            <h1>حواسيب<br />
                بمُختَلَف المواصفات</h1>
        </div>
        <br />
        <div class="btn-search">
            <a href="products.aspx" title="Eshop Products" class="btn-dec">تصفُّح المنتجات</a>
        </div>
    </div>
</asp:Content>

