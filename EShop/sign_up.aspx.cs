﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void SignupBtn_Click(object sender, EventArgs e)
    {
        bool number = IsDigitsOnly(mobileTB.Text);
        bool validator = true;
        if (usernameTB.Text == "")
        {
            BoxValidator(usernameTB, 1);
            validator = false;
        }
        else
        {
            BoxValidator(usernameTB, 2);
        }
        if (!emailTB.Text.Contains("@") && !emailTB.Text.Contains("."))
        {
            BoxValidator(emailTB, 1);
            validator = false;
        }
        else
        {
            BoxValidator(emailTB, 2);

        }
        if (passwordTB.Text == "")
        {
            BoxValidator(passwordTB, 1);
            validator = false;
        }
        else
        {
            BoxValidator(passwordTB, 2);
        }
        if (addressTB.Text == "")
        {
            BoxValidator(addressTB, 1);
            validator = false;
        }
        else
        {
            BoxValidator(addressTB, 2);
        }
        if (number == false)
        {
            BoxValidator(mobileTB, 1);
            validator = false;
        }
        else
        {
            BoxValidator(addressTB, 2);
        }
        if (validator == false)
        {
            validatorrr.Text = "الرجاء التحقق من ادخال المعلومات المطلوبة";
        }
        else
        {
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
            {
                Account new_client = new Account(username: usernameTB.Text, email: emailTB.Text, password: passwordTB.Text, address: addressTB.Text);

                /*
                string query = "INSERT INTO Accounts (username, email, password, address, mobile_number, is_admin) VALUES (@username, @email, @password, @address, @mobile_number, @is_admin)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@username", usernameTB.Text);
                cmd.Parameters.AddWithValue("@email", emailTB.Text);
                cmd.Parameters.AddWithValue("@password", passwordTB.Text);
                cmd.Parameters.AddWithValue("@address", addressTB.Text);
                cmd.Parameters.AddWithValue("@mobile_number", mobileTB.Text);
                cmd.Parameters.AddWithValue("@is_admin", "no");
                con.Open();
                int i = cmd.ExecuteNonQuery();
                */
                if (new_client.save())
                {
                    validatorrr.Text = "تم تسجيل حساب جديد";
                    Response.Redirect("sign_in.aspx");
                }
                else
                {
                    validatorrr.Text = "جرب بريد اخر";
                }


            }
        }

    }
    protected void BoxValidator(TextBox textboxName, int num)
    {
        if (num == 1)
        {
            textboxName.CssClass = "box danger";
        }
        else
        {
            textboxName.CssClass = "box";
        }

    }
    bool IsDigitsOnly(string str)
    {
        foreach (char c in str)
        {
            if (c < '0' || c > '9')
                return false;
        }

        return true;
    }
}