﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="account.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>المستخدم&nbsp;|&nbsp;EShop</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <div class="main inside-main text-right accounts-div"  style="direction:rtl; padding-right:20px;">
        <br /><br />
        <div class="row">
            <div class="col-md-6" style="margin:auto;">
                <label>رقم الحساب :</label>
                <asp:Label runat="server" ID="IdLabel" Text=""></asp:Label>
            </div>
            <div class="col-md-6">
                <label>اسم الحساب :</label>
                <asp:TextBox runat="server" ID="NameTB" Text="" CssClass="input"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label>الايميل :</label>
                <asp:TextBox runat="server" ID="EmailTB" Text="" CssClass="input"></asp:TextBox>
            </div>
            <div class="col-md-6">
                <label>كلمة المرور :</label>
                <asp:TextBox runat="server" ID="PasswordTB" Text="" CssClass="input" TextMode="Password"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label>العنوان :</label>
                <asp:TextBox runat="server" ID="AddressTB" Text="" CssClass="input"></asp:TextBox>
            </div>
            <div class="col-md-6">
                <label>رقم الهاتف :</label>
                <asp:TextBox runat="server" ID="MobileTB" Text="" CssClass="input"></asp:TextBox>
            </div>
        </div>
        <div class="row" runat="server" id="AdminLabelDiv" visible="false">
            <div class="col-md-6">
                <label>هل الحساب مدير؟</label>
                <asp:DropDownList runat="server" ID="AdminDDL">
                    <asp:ListItem Text="True"></asp:ListItem>
                    <asp:ListItem Text="False"></asp:ListItem>
                </asp:DropDownList>
            </div>
            
        </div>
        <div class="row" style="text-align:center;">
            <div class="col-md-12">
                <asp:Button runat="server" ID="UpdateAccount" Text="تعديل" CssClass="submit-btn" OnClick="UpdateAccount_Click"/>
                <br />
                <asp:Label runat="server" ID="testlabel" Text=""></asp:Label>
            </div>
            
            
        </div>
        <br /><br />

    </div>
</asp:Content>

