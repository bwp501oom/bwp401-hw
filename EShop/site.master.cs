﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class site : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] != null)
        {
            LoginAndSignup.Visible = false;
            LoginAndLogout.Visible = true;
            AccountName.InnerText = Session["username"].ToString();


            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
            {
                con.Open();
                string query = "SELECT Id FROM Accounts WHERE username = @username ";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@username", Session["username"].ToString());
                string id = Convert.ToString(cmd.ExecuteScalar());
                if (id.Length > 0)
                {
                    AccountSettings.HRef += id;
                    string query2 = "SELECT is_admin FROM Accounts WHERE Id = @Id";
                    SqlCommand cmd2 = new SqlCommand(query2, con);
                    cmd2.Parameters.AddWithValue("@Id", id);
                    string admin = Convert.ToString(cmd2.ExecuteScalar());
                    AccountSettings.HRef += "&is_admin=" + admin;
                    if (admin == "True")
                    {
                        AllAccounts.Visible = true;
                        SalesLink.Visible = true;
                        WebsiteMessages.Visible = true;
                    }
                }
                else
                {
                }
                con.Close();
            }
        }
        else
        {
            LoginAndLogout.Visible = false;
            LoginAndSignup.Visible = true;
        }
    }
    protected void Logout_Click(object sender, EventArgs e)
    {
        if(Session["username"]!=null) Session.Abandon();
        Response.Redirect("index.aspx");
    }
}
