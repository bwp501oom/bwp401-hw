﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="sign_up.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>إنشاء حساب&nbsp;|&nbsp;EShop</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <div class="main inside-main">
        <div class="mid">
            إنشاء حساب
        </div>
        <div class="sign-up-form2">
            <div class="mid">
                <img src="pics/Signup_icon.svg" alt="" style="width: 47.28px; height: 50.66px;" />
            </div>
            <div class="mid-form">
                <table class="sign-in-table">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" CssClass="box" ID="usernameTB"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="&ensp;&ensp;اسم المُستخدم"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox runat="server" CssClass="box" ID="emailTB" placeholder="example@example.com"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="&ensp;&ensp;البريد الإلكتروني"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox runat="server" CssClass="box" ID="passwordTB" TextMode="Password"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="&ensp;&ensp;كلمة السر"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox runat="server" CssClass="box" ID="mobileTB"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="&ensp;&ensp;رقم الهاتف"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox runat="server" CssClass="box" ID="addressTB"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="&ensp;&ensp;العنوان"></asp:Label>
                        </td>
                    </tr>

                </table>
                
                <p class="accept-align">
                    أوافق على
							  <a href="about.aspx" class="underline">شروط وسياسة الشركة</a>
                    <input type="checkbox" name="accept" value="" required="required" runat="server" id="checkbox1"/>
                </p>
                <asp:Button runat="server" ID="SignupBtn" CssClass="submit-btn" Text="تسجيل" OnClick="SignupBtn_Click"/>
                <br />
                <asp:Label runat="server" ID="validatorrr" Text="" CssClass="warningg"></asp:Label>
                <br />
            </div>
        </div>
    </div>
</asp:Content>

