﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="contact_us.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>اتصل بنا&nbsp;|&nbsp;EShop</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
    <div class="main inside-main">
			<div class="mid">
				  	اتصل بنا
			  </div>
			  <div class="sign-up-form" style="height:750px;">
				  <div class="mid">
					  <img src="pics/contact_us_icon.svg" alt=""  style="width:47.28px;height:50.66px;" />
				  </div>
			  	  <div class = "mid-form">
					    <table class="sign-in-table">
							  <tr>
                                
								<td><asp:TextBox runat="server" ID="NameTB" CssClass="input" required="required"></asp:TextBox></td>
								<td>&ensp;&ensp;الاسم</td>
							  </tr>	
							  <tr>
								<td><asp:TextBox runat="server" ID="EmailTB" CssClass="input" required="required" placeholder="example.anything.com"></asp:TextBox></td>
								<td>&ensp;&ensp;البريد الإلكتروني</td>
							  </tr>	
							  
						  </table>
					    <p>نص الرسالة</p>
                        
						  <asp:TextBox runat="server" ID="ContentTA" required="required"  maxlength="600" CssClass="input" style="resize:none;" placeholder="اكتب رسالتك هنا" BorderStyle="Inset" TextMode="MultiLine" height="200px" Width="500px" >	
						</asp:TextBox>
                        
						<asp:Button runat="server" ID="SendMessage" CssClass="submit-btn" Text="ارسال" OnClick="SendMessage_Click"/>
                        <br />
                        <asp:Label runat="server" ID="MessageLB" Visible="true"></asp:Label>
						 <br/>
				  </div>
		    </div>
			</div>
</asp:Content>

