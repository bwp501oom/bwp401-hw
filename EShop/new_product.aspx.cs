﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Configuration;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] == null)
        {
            //Response.Redirect("index.aspx");
        }
    }
    protected void ProductTypeDDL_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ProductTypeDDL.SelectedValue == "pc" ||ProductTypeDDL.SelectedValue == "laptop")
        {
            NewPcTable.Visible = true;
            NewPrinterTable.Visible = false;
            NewScannerTable.Visible = false;
            printerIsScanner.Visible = false;
        }
        if (ProductTypeDDL.SelectedValue == "printer")
        {
            NewPcTable.Visible = false;
            NewPrinterTable.Visible = true;
            NewScannerTable.Visible = false;
        }
        if (ProductTypeDDL.SelectedValue == "scanner")
        {
            NewPrinterTable.Visible = false;
            NewPcTable.Visible = false;
            NewScannerTable.Visible = true;
            printerIsScanner.Visible = false;
        }
    }
    protected void printerTypeDDL_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (printerTypeDDL.SelectedValue == "scanner")
        {
            printerIsScanner.Visible = true;
        }
        else
        {
            printerIsScanner.Visible = false;
        }
    }
    protected void InsertProductBtn_Click(object sender, EventArgs e)
    {
        bool stock = IsDigitsOnly(productStock.Text);
        if (ProductTypeDDL.SelectedValue == null || productNameTB.Text.Length <= 0 || productYearTB.Text.Length <= 0 || productBrandTB.Text.Length <= 0 || productColor.Text.Length <= 0 || productPrice.Text.Length <= 0 || imagePath.Text.Length <= 0 || productStock.Text.Length <= 0)
        {
            
            validatorrr.Text = "الرجاء التاكد من ادخال البيانات الاساسية";
            if (stock == true && Convert.ToInt32(productStock.Text) < 1)
            {
                validatorrr.Text = "الرجاء التأكد من ادخال كمية المستودع بشكل صحيح.";
            }
        }
        else
        {
            
            bool year = IsDigitsOnly(productYearTB.Text);
            bool price = IsDigitsOnly(productPrice.Text);
            
            if (year == true && price == true && stock == true)
            {

                try
                {
                    using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
                    {
                        string query = "INSERT INTO Products (product_name, product_type, product_brand, product_year, price, color, image, stock, ";
                        con.Open();
                        if (ProductTypeDDL.SelectedValue == "pc" || ProductTypeDDL.SelectedValue == "laptop")
                        {

                            query += "pc_storage, pc_ram, pc_processor, pc_resolution, pc_model, pc_gpu) VALUES (@product_name, @product_type, @product_brand, @product_year, @price, @color, @image, @stock, @pc_storage, @pc_ram, @pc_processor, @pc_resolution, @pc_model, @pc_gpu)";
                            SqlCommand cmd = new SqlCommand(query, con);
                            cmd.Parameters.AddWithValue("@product_name", productNameTB.Text);
                            cmd.Parameters.AddWithValue("@product_type", ProductTypeDDL.SelectedValue);
                            cmd.Parameters.AddWithValue("@product_brand", productBrandTB.Text);
                            cmd.Parameters.AddWithValue("@product_year", productYearTB.Text);
                            cmd.Parameters.AddWithValue("@price", productPrice.Text);
                            cmd.Parameters.AddWithValue("@color", productColor.Text);
                            cmd.Parameters.AddWithValue("@image", imagePath.Text);
                            cmd.Parameters.AddWithValue("@stock", productStock.Text);

                            cmd.Parameters.AddWithValue("@pc_storage", pcStorageTB.Text);
                            cmd.Parameters.AddWithValue("@pc_ram", pcRamTB.Text);
                            cmd.Parameters.AddWithValue("@pc_processor", pcCpuTB.Text);
                            cmd.Parameters.AddWithValue("@pc_resolution", pcResolutionTB.Text);
                            cmd.Parameters.AddWithValue("@pc_model", pcModelTB.Text);
                            cmd.Parameters.AddWithValue("@pc_gpu", pcGpuTB.Text);
                            int i = cmd.ExecuteNonQuery();
                            if (i != 0)
                            {
                                validatorrr.Text = "تم اضافة منتج جديد";

                            }
                            else
                            {
                                validatorrr.Text = "خطأ";
                            }
                        }
                        else if (ProductTypeDDL.SelectedValue == "printer")
                        {
                            query += "printer_resolution, printer_speed, printer_paper, printer_type";
                            if (printerTypeDDL.SelectedValue == "scanner")
                            {
                                query += ",printer_memory)";
                            }
                            else
                            {
                                query += ")";
                            }
                            query += " VALUES (@product_name, @product_type, @product_brand, @product_year, @price, @color, @image, @stock, @printer_resolution, @printer_speed, @printer_paper, @printer_type";
                            if (printerTypeDDL.SelectedValue == "scanner")
                            {
                                query += ", @printer_memory)";
                            }
                            else
                            {
                                query += ")";
                            }
                            SqlCommand cmd = new SqlCommand(query, con);
                            cmd.Parameters.AddWithValue("@product_name", productNameTB.Text);
                            cmd.Parameters.AddWithValue("@product_type", ProductTypeDDL.SelectedValue);
                            cmd.Parameters.AddWithValue("@product_brand", productBrandTB.Text);
                            cmd.Parameters.AddWithValue("@product_year", productYearTB.Text);
                            cmd.Parameters.AddWithValue("@price", productPrice.Text);
                            cmd.Parameters.AddWithValue("@color", productColor.Text);
                            cmd.Parameters.AddWithValue("@image", imagePath.Text);
                            cmd.Parameters.AddWithValue("@stock", productStock.Text);

                            cmd.Parameters.AddWithValue("@printer_resolution", printerResolutionTB.Text);
                            cmd.Parameters.AddWithValue("@printer_speed", printerSpeedTB.Text);
                            cmd.Parameters.AddWithValue("@printer_paper", printerPaperTB.Text);
                            cmd.Parameters.AddWithValue("@printer_type", printerTypeDDL.SelectedValue);
                            if (printerTypeDDL.SelectedValue == "scanner")
                            {
                                cmd.Parameters.AddWithValue("@printer_memory", printerIsScannerMemory.Text);
                            }

                            int i = cmd.ExecuteNonQuery();
                            if (i != 0)
                            {
                                validatorrr.Text = "تم اضافة منتج جديد";

                            }
                            else
                            {
                                validatorrr.Text = "خطأ";
                            }
                        }
                        else if (ProductTypeDDL.SelectedValue == "scanner")
                        {
                            query += "scanner_type, scanner_speed, scanner_paper, scanner_resolution) VALUES (@product_name, @product_type, @product_brand, @product_year, @price, @color, @image, @stock, @scanner_type, @scanner_speed, @scanner_paper, @scanner_resolution)";
                            SqlCommand cmd = new SqlCommand(query, con);
                            cmd.Parameters.AddWithValue("@product_name", productNameTB.Text);
                            cmd.Parameters.AddWithValue("@product_type", ProductTypeDDL.SelectedValue);
                            cmd.Parameters.AddWithValue("@product_brand", productBrandTB.Text);
                            cmd.Parameters.AddWithValue("@product_year", productYearTB.Text);
                            cmd.Parameters.AddWithValue("@price", productPrice.Text);
                            cmd.Parameters.AddWithValue("@color", productColor.Text);
                            cmd.Parameters.AddWithValue("@image", imagePath.Text);
                            cmd.Parameters.AddWithValue("@stock", productStock.Text);

                            cmd.Parameters.AddWithValue("@scanner_type", ScannerTypeDDL.SelectedValue);
                            cmd.Parameters.AddWithValue("@scanner_speed", scannerSpeed.Text);
                            cmd.Parameters.AddWithValue("@scanner_paper", scannerPaper.Text);
                            cmd.Parameters.AddWithValue("@scanner_resolution", scannerResolution.Text);
                            int i = cmd.ExecuteNonQuery();
                            if (i != 0)
                            {
                                validatorrr.Text = "تم اضافة منتج جديد";

                            }
                            else
                            {
                                validatorrr.Text = "خطأ";
                            }
                        }
                        string query2 = "INSERT INTO Sales (product_name, image, quantity, total_sales) VALUES (@product_name, @image, @quantity, @total_sales)";
                        SqlCommand cmd2 = new SqlCommand(query2, con);
                        cmd2.Parameters.AddWithValue("@product_name", productNameTB.Text);
                        cmd2.Parameters.AddWithValue("@image", imagePath.Text);
                        cmd2.Parameters.AddWithValue("@quantity", "0");
                        cmd2.Parameters.AddWithValue("@total_sales", productPrice.Text);
                        cmd2.ExecuteNonQuery();
                        con.Close();
                        Response.Redirect("products.aspx");
                    }
                }
                catch (Exception ex)
                {
                    validatorrr.Text = "الرجاء ادخال اسم منتج مختلف";
                }
                
            }
            else
            {
                validatorrr.Text = "الرجاء التاكد من ادخال السعر او سنة الانتاج او كمية المخزن كارقام فقط";
            }

            
        }
        
    }
    bool IsDigitsOnly(string str)
    {
        foreach (char c in str)
        {
            if (c < '0' || c > '9')
                return false;
        }

        return true;
    }
    protected void Unnamed_Click(object sender, EventArgs e)
    {

        string strFileName;
        string strFilePath;
        string strFolder;
        strFolder = Server.MapPath("~/pics/ProductImages/");
        strFileName = oFile.PostedFile.FileName;
        strFileName = Path.GetFileName(strFileName);
        strFilePath = strFolder + strFileName;
        oFile.PostedFile.SaveAs(strFilePath);
        ImageImg.Visible = true;
        imagePath.Text = "pics/ProductImages/" + strFileName;
        ImageImg.Src = imagePath.Text;
    }
}