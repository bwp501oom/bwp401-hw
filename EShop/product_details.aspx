﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="product_details.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>معلومات المنتج&nbsp;|&nbsp;EShop</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <div class="main inside-main">
        <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lbltest" runat="server" Visible="false"></asp:Label>
        <div class="row text-center" runat="server" id="DeleteProductDiv" visible="false">
            <div class="col-md-12">
                <asp:Button runat="server" ID="DeleteProduct" OnClick="DeleteProduct_Click" Text="حذف المنتج" CssClass="danger-btn-edited" OnClientClick="return confirm('هل انت متأكد من حذف هذا المنتج؟');" />
                <asp:Button runat="server" ID="EditProduct" Text="تعديل المنتج" CssClass="submit-btn-edited" OnClick="EditProduct_Click" />
            </div>
        </div>
        <br />
        <br />
        <br />
        <div style="width: 80%; direction: rtl; margin: 0px auto;" class="text-center">
            <div class="row text-center">
                <div class="col-md-12">
                    <asp:Label runat="server" ID="validatorr" Text=""></asp:Label>
                </div>
            </div>
            <br />
            <br />
            <div class="row">
                <div class="col-md-6">
                    <label>اسم المنتج :</label>
                    <asp:TextBox runat="server" ID="productName" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label>نوع المنتج :</label>
                    <asp:TextBox runat="server" ID="productType" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                    <asp:Label runat="server" ID="HiddenProductType" Text="" Visible="false"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-6">
                    <label>لون المنتج :</label>
                    <asp:TextBox runat="server" ID="productColor" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label>طراز المنتج :</label>
                    <asp:TextBox runat="server" ID="productBrand" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-6">
                    <label>سنة التصنيع :</label>
                    <asp:TextBox runat="server" ID="productYear" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="col-md-6">
                    <label>سعر المنتج :</label>
                    <asp:TextBox runat="server" ID="productPrice" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                    <label>ليرة سورية</label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-6" runat="server" id="InStockDiv">
                    <label>كمية المخزن :</label>
                    <asp:TextBox runat="server" ID="productStock" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="col-md-6" runat="server" id="OutOfStockDiv" visible="false">
                    <asp:Label runat="server" ID="OutOfStockLabel" Text="لقد نفذ المنتج من المخزن"></asp:Label>
                </div>
            </div>
            <div class="row" runat="server" id="ProductOrderDiv" visible="false">
                <div class="col-md-6">
                    <div>
                        <asp:Button runat="server" ID="QuantityPlus" Text="+" OnClick="QuantityPlus_Click" CssClass="submit-btn-edited" />
                        <asp:TextBox runat="server" ID="QuantityTB" Text="" ReadOnly="true" CssClass="box"></asp:TextBox>
                        <asp:Button runat="server" ID="QuantityMinus" Text="-" OnClick="QuantityMinus_Click" CssClass="danger-btn-edited" />
                        <label>السعر الاجمالي :</label>
                        <asp:Label runat="server" Text="" ID="TotalPriceLabel"></asp:Label>
                    </div>
                </div>
                <div class="col-md-6">
                    <asp:Button runat="server" Text="اضافة الى سلة الشراء" ID="Button1" OnClick="SubmitItemOrder_Click" CssClass="submit-btn-edited" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 text-right" style="margin: auto 0px; padding-right: 120px;" runat="server" id="ExtraDetailsDiv" visible="true">
                    <label>معلومات اخرى :</label>
                </div>
                <div class="col-md-8 text-reset" runat="server" id="EditImageDiv" visible="false">
                    <div class="product-image-div">
                        <asp:Button runat="server" CssClass="add-product-img-btn" Text="ارفع" OnClick="Unnamed_Click" />&emsp;&emsp;&emsp;
                    <input id="oFile" type="file" runat="server" name="oFile" />
                        <asp:Label runat="server" Text="" ID="imagePath" Visible="true"></asp:Label>
                    </div>
                </div>
                <div class="col-md-4">
                    <img src="" style="width: 100%;" runat="server" id="productImage" />
                </div>
            </div>
            
            <div id="pcDiv" runat="server" visible="false">
                <div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>سعة التخزين :</label>
                            <asp:TextBox runat="server" ID="pcStorage" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <label>الرام : </label>
                            <asp:TextBox runat="server" ID="pcRam" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-6">
                            <label>المعالج :</label>
                            <asp:TextBox runat="server" ID="pcProcessor" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <label>كرت الشاشة :</label>
                            <asp:TextBox runat="server" ID="pcGpu" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-6">
                            <label>دقة الشاشة :</label>
                            <asp:TextBox runat="server" ID="pcResolution" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <label>الموديل :</label>
                            <asp:TextBox runat="server" ID="pcModel" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div id="printerDiv" runat="server" visible="false">
                <div class="row">
                    <div class="col-md-6">
                        <label>دقة الطابعة :</label>
                        <asp:TextBox runat="server" ID="printerResolution" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                        <label>سرعة الطابعة :</label>
                        <asp:TextBox runat="server" ID="printerSpeed" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-6">
                        <label>حجم الورق :</label>
                        <asp:TextBox runat="server" ID="printerPaper" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                        <label>نوع الطابعة : </label>
                        <asp:TextBox runat="server" ID="printerType" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <br />
                <div class="row" runat="server" id="printerMemoryDiv" visible="false">
                    <div class="col-md-6">
                        <label>ذاكرة الطابعة :</label>
                        <asp:TextBox runat="server" ID="printerMemory" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="" id="scannerDiv" runat="server" visible="false">
                <div class="row">
                    <div class="col-md-6">
                        <label>نوع الماسح :</label>
                        <asp:TextBox runat="server" ID="scannerType" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                        <label>سرعة المسح :</label>
                        <asp:TextBox runat="server" ID="scannerSpeed" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-6">
                        <label>حجم الورق :</label>
                        <asp:TextBox runat="server" ID="scannerPaper" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                        <label>دقة الماسح :</label>
                        <asp:TextBox runat="server" ID="scannerResolution" CssClass="label-box" Text="" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <br />
            </div>
        </div>
        <div class="row" runat="server" id="UpdateProductDiv" visible="false">
            <div class="col-md-12 text-center">
                <asp:Button runat="server" ID="UpdateProduct" Text="تعديل" CssClass="submit-btn-edited" OnClick="UpdateProduct_Click" />
            </div>
        </div>

        <div class="row" runat="server" visible="false">
            <asp:Label runat="server" ID="product_id" Text=""></asp:Label>
            <asp:Label runat="server" ID="product_image" Text=""></asp:Label>
            <asp:Label runat="server" ID="product_name" Text=""></asp:Label>
        </div>
        <br />
        <br />
        <br />
        <br />
        <asp:Label runat="server" ID="testttttttt" Text="asd;amdslkansdasd" Visible="false"></asp:Label>
    </div>
</asp:Content>

