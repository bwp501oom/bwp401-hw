﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="new_product.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>منتج جديد&nbsp;|&nbsp;EShop</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
   <div class="main inside-main">
        <div class="mid">
				  	إضافة منتج
		</div>
        <div class="new-product-form">
            <%--general product info--%>
            <div class="mid">
					  <img src="pics/new_product.svg" alt="" width="47.28" 	height="50.66"/>
		    </div>
            <div class = "mid-products-form">
                <asp:Table runat="server" ID="NewProductTableMain" CssClass="new-product-table">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList ID="ProductTypeDDL" runat="server" CssClass="box" AutoPostBack="true" OnSelectedIndexChanged="ProductTypeDDL_SelectedIndexChanged">
                                <asp:ListItem Enabled="true" Text="اختر نوع المنتج"></asp:ListItem>
                                <asp:ListItem Text="حاسب" Value="pc"></asp:ListItem>
                                <asp:ListItem Text="حاسب محمول" Value="laptop"></asp:ListItem>
                                <asp:ListItem Text="طابعة" Value="printer"></asp:ListItem>
                                <asp:ListItem Text="ماسح ضوئي" Value="scanner"></asp:ListItem>
                            </asp:DropDownList>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;نوع المنتج&ensp;&ensp;"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="productNameTB"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;اسم المنتج"></asp:Label>
                        </asp:TableCell>

                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="productYearTB"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;سنة الانتاج&ensp;&ensp;"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="productBrandTB"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;ماركة المنتج"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="productColor"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;لون المنتج&ensp;&ensp;"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="productPrice"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;سعر المنتج"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox runat="server" ID="productStock" Text="1" CssClass="box"></asp:TextBox>
                            
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label runat="server"  Text="كمية المخزن"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <%--if product is pc or laptop--%>
                <asp:Table runat="server" ID="NewPcTable" Visible="false" CssClass="new-product-table">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box2" ID="pcModelTB"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;موديل&ensp;&ensp;"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="pcCpuTB"></asp:TextBox>&nbsp;
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;المعالج"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box2" ID="pcStorageTB"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;حجم التخزين بالجيجا&ensp;&ensp;"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="pcRamTB"></asp:TextBox>&nbsp;
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;الرام"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box2" ID="pcGpuTB"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;كرت الشاشة&ensp;&ensp;"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="pcResolutionTB"></asp:TextBox>&nbsp;
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;دقة الشاشة"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <%--if product is printer--%>
                <asp:Table runat="server" ID="NewPrinterTable" Visible="false" CssClass="new-product-table">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box2" ID="printerSpeedTB"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="السرعة&ensp;&ensp;"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="printerResolutionTB"></asp:TextBox>&nbsp;
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;دقة الطابعة"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList ID="printerTypeDDL" runat="server" CssClass="box2" AutoPostBack="true" OnSelectedIndexChanged="printerTypeDDL_SelectedIndexChanged">
                                <asp:ListItem Enabled="true" Text="اختر نوع الطابعة"></asp:ListItem>
                                <asp:ListItem Text="كبيرة مع ماسح ضوئي" Value="scanner"></asp:ListItem>
                                <asp:ListItem Text="صغيرة مكتبية" Value="printer"></asp:ListItem>
                            </asp:DropDownList>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;نوع الطابعة"></asp:Label>&nbsp;&nbsp;&nbsp;
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="printerPaperTB"></asp:TextBox>&nbsp;
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;حجم الورق"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <%--if product is scanner--%>
                <asp:Table runat="server" ID="NewScannerTable" Visible="false" CssClass="new-product-table">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="scannerPaper"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="حجم الورق&ensp;&ensp;"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="scannerSpeed"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&nbsp;&nbsp;السرعة"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:DropDownList ID="ScannerTypeDDL" runat="server" CssClass="box" AutoPostBack="true">
                                <asp:ListItem Enabled="true" Text="اختر نوع الماسح"></asp:ListItem>
                                <asp:ListItem Text="مسطح" Value="flat"></asp:ListItem>
                                <asp:ListItem Text="دفعي" Value="sheet"></asp:ListItem>
                                <asp:ListItem Text="شرائحي" Value="convenience"></asp:ListItem>
                                <asp:ListItem Text="متعدد الوظائف" Value="multi"></asp:ListItem>
                            </asp:DropDownList>
                        </asp:TableCell>
                        <asp:TableCell>&nbsp;&nbsp;&nbsp;
                        <asp:Label runat="server" Text="نوع الماسح&ensp;&ensp;"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="scannerResolution"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;دقة المسح"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <%--if printer is also scanner--%>
                <asp:Table runat="server" ID="printerIsScanner" Visible="false" CssClass="new-product-table">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox runat="server" CssClass="box" ID="printerIsScannerMemory"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                        <asp:Label runat="server" Text="&ensp;&ensp;سعة التخزين"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <div style="margin-bottom: 44px;"></div>
            </div>
                <div class="product-image-div">
                    <img  runat="server" id="ImageImg" Visible="false" class="product-image" src="" style="width:400px;"/>
                    
                    <asp:Button  runat="server" CssClass="add-product-img-btn" Text="ارفع" OnClick="Unnamed_Click"/>&emsp;&emsp;&emsp;
                    <input id="oFile" type="file" runat="server" name="oFile"/>
					:إضافة صورة للمنتج
                    
                    <asp:Label runat="server" Text="" ID="imagePath" Visible="false"></asp:Label>
                </div>
                
                 <div>
                    <asp:Button runat="server" ID="InsertProductBtn" CssClass="submit-btn" Text="ادخال" OnClick="InsertProductBtn_Click"/>
                    <br />
                    <asp:Label runat="server" Text="" ID="validatorrr" CssClass="warning"></asp:Label>
                </div>
        </div>
    </div>
</asp:Content>

