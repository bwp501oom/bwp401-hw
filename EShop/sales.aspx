﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="sales.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>المبيعات&nbsp;|&nbsp;EShop</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <div class="main inside-main" style="direction: rtl;">
        <br />
        <div class="row text-center">
            <div class="col-md-4">
                <asp:Button runat="server" ID="AllSales" OnClick="AllSales_Click" Text="جميع المبيعات" CssClass="submit-btn-edited"/>
            </div>
            <div class="col-md-4">
                <asp:Button runat="server" ID="GetLastWeekSales" OnClick="GetLastWeekSales_Click" Text="مبيعات الاسبوع الماضي" CssClass="submit-btn-edited"/>
            </div>
            <div class="col-md-4">
                <asp:Button runat="server" ID="GetLastMonthSales" OnClick="GetLastMonthSales_Click" Text="مبيعات الشهر الماضي" CssClass="submit-btn-edited"/>
            </div>
        </div>
        <br />
        <div class="row text-center">
            <div class="col-md-12">
                <label>او يمكنك البحث عن فترة معينة</label>
            </div>
            <div class="col-md-6">
                <label>من تاريخ</label>
                <asp:TextBox runat="server" ID="FromDate" TextMode="Date" CssClass="box"></asp:TextBox>
                <asp:Label runat="server" ID="TestingDate" Text=""></asp:Label>
            </div>
            <div class="col-md-6">
                <label>الى تاريخ</label>
                <asp:TextBox runat="server" ID="ToDate" TextMode="Date" CssClass="box"></asp:TextBox>
            </div>
            <div class="col-md-12">
                <asp:Button runat="server" ID="GivenDateSearch" CssClass="submit-btn-edited" Text="ابحث" OnClick="GivenDateSearch_Click"/>
            </div>
        </div>
        <br />
        <br />
        <div class="row">
            <div class="col-md-12" style="text-align: center; font-size: 30px;">
                <label>فواتير المستخدمين</label>
            </div>
        </div>
        <div class="row text-center" style="margin: auto; background-color: #232F3E; color: white; font-size: 24px;">
            <div class="col-md-2">
                <label>رقم الطلب</label>
            </div>
            <div class="col-md-2">
                <label>اسم الحساب</label>
            </div>
            <div class="col-md-3">
                <label>السعر الاجمالي</label>
            </div>
            <div class="col-md-1">
                <label>الحالة</label>
            </div>
            <div class="col-md-1">
                <label>التفاصيل</label>
            </div>
            <div class="col-md-3">
                <label>التاريخ</label>
            </div>
        </div>
        <asp:Repeater runat="server" ID="Repeater2">
            <ItemTemplate>
                <div class="row  text-center" style="margin: auto;">
                    <div class="col-md-2">
                        <asp:Label runat="server" Text='<%#Eval("order_id")%>' ID="OrderID"></asp:Label>
                        <asp:Label runat="server" Text='<%#Eval("Id")%>' Visible="false"></asp:Label>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("username")%>'></asp:Label>
                    </div>
                    <div class="col-md-3" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("order_price")%>'></asp:Label>
                    </div>
                    <div class="col-md-1" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("status")%>'></asp:Label>
                    </div>
                    <div class="col-md-1" style="margin: auto;">
                        <a runat="server" href='<%# Eval("order_id", "~/order_details.aspx?Id={0}") %>'>التفاصيل</a>
                    </div>
                    <div class="col-md-3" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("date_ordered")%>'></asp:Label>
                    </div>
                </div>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <div class="row  text-center" style="margin: auto; background-color: #EFF0F1;">
                    <div class="col-md-2">
                        <asp:Label runat="server" Text='<%#Eval("order_id")%>' ID="OrderID"></asp:Label>
                        <asp:Label runat="server" Text='<%#Eval("Id")%>' Visible="false"></asp:Label>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("username")%>'></asp:Label>
                    </div>
                    <div class="col-md-3" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("order_price")%>'></asp:Label>
                    </div>
                    <div class="col-md-1" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("status")%>'></asp:Label>
                    </div>
                    <div class="col-md-1" style="margin: auto;">
                        <a runat="server" href='<%# Eval("order_id", "~/order_details.aspx?Id={0}") %>'>التفاصيل</a>
                    </div>
                    <div class="col-md-3" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("date_ordered")%>'></asp:Label>
                    </div>
                </div>
            </AlternatingItemTemplate>
        </asp:Repeater>
        <div class="row">
             <div class="col-md-12 text-center">
                <asp:Button runat="server" ID="PrintBtn" Text="طباعة" CssClass="submit-btn-edited" OnClick="PrintBtn_Click" />
            </div>
        </div>
        <br /><br />
        <div class="row">
            <div class="col-md-12" style="text-align: center; font-size: 30px;">
                <label>المبيعات لكل منتج</label>
            </div>
        </div>
        <div class="row text-center" style="margin: auto; background-color: #232F3E; color: white; font-size: 24px;">
            <div class="col-md-5">
                <label>صورة المنتج</label>
            </div>
            <div class="col-md-2">
                <label>اسم المنتج</label>
            </div>
            <div class="col-md-2">
                <label>كمية البيع</label>
            </div>
            <div class="col-md-3">
                <label>السعر الاجمالي</label>
            </div>
        </div>
        <asp:Repeater runat="server" ID="Repeater1">
            <ItemTemplate>
                <div class="row  text-center" style="margin: auto;">
                    <div class="col-md-5">
                        <img class="text-right" src='<%#Eval("image")%>' alt="Card image cap" style="width: 40%;">
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("product_name")%>'></asp:Label>
                        <asp:Label runat="server" Text='<%#Eval("Id")%>' Visible="false" ID="SalesID"></asp:Label>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("quantity")%>'></asp:Label>
                    </div>
                    <div class="col-md-3" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("total_sales")%>'></asp:Label>
                        <label>ليرة سورية</label>
                    </div>
                </div>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <div class="row  text-center" style="margin: auto; background-color: #EFF0F1;">
                    <div class="col-md-5">
                        <img class="text-right" src='<%#Eval("image")%>' alt="Card image cap" style="width: 40%;">
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("product_name")%>'></asp:Label>
                        <asp:Label runat="server" Text='<%#Eval("Id")%>' Visible="false" ID="SalesID"></asp:Label>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("quantity")%>'></asp:Label>
                    </div>
                    <div class="col-md-3" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("total_sales")%>'></asp:Label>
                        <label>ليرة سورية</label>
                    </div>
                </div>
            </AlternatingItemTemplate>
        </asp:Repeater>
        <div class="row">
             <div class="col-md-12 text-center">
                <asp:Button runat="server" ID="PrintBtn2" Text="طباعة" CssClass="submit-btn-edited" OnClick="PrintBtn2_Click" />
            </div>
        </div>
        <br /><br />
        
        <asp:Label runat="server" ID="TestLabel" Text="" Visible="false"></asp:Label>
            <div style="color:white; border-color:white;"  class="noselect">
            <asp:Panel runat="server" ID="printPanel" Direction="RightToLeft" CssClass="text-center" style="display:none;">
                <p  style="font-size:50px;">EShop</p>
                <br /><br /><br />
                <p style="font-size:40px;">Sales Report</p>
                <br />
                <asp:Label runat="server" ID="fromDatePdf" Text="----/--/--"></asp:Label><br />
                to<asp:Label runat="server" ID="ToDatePdf" Text="----/--/--"></asp:Label><br />
                <asp:GridView runat="server" ID="gridviewData" CssClass="Grid">
                    <HeaderStyle/>
                </asp:GridView>
                
                
                <br />
            </asp:Panel>
                <div style="color:white; border-color:white;" class="noselect">
            <asp:Panel runat="server" ID="printPanel2" Direction="RightToLeft" CssClass="text-center"  style="display:none;">
                <p  style="font-size:50px;">EShop</p>
                <br /><br /><br />
                <p style="font-size:40px;">Total Sales</p>
                <br />
                <asp:GridView runat="server" ID="gridview1" CssClass="Grid">
                    <HeaderStyle/>
                </asp:GridView>
                <p>Total Price in SYP:</p>
                <asp:Label runat="server" ID="total" Text=""></asp:Label>
                <br />
            </asp:Panel>
        </div>
        </div>
    </div>

    

</asp:Content>

