﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="messagedetails.aspx.cs" Inherits="messagedetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>رسالة:<%#Eval("id") %></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <div class="main inside-main" style="direction: rtl;">
        <br />
        <br />
        <div class="row text-center" style="margin: auto; background-color: #232F3E; color: white; font-size: 24px;">
            <div class="col-md-2">
                <label>رقم الرسالة</label>
            </div>
            <div class="col-md-2">
                <label>اسم الحساب</label>
            </div>
            <div class="col-md-2">
                <label>الايميل</label>
            </div>
            <div class="col-md-5">
                <label>الرسالة</label>
            </div>
            <div class="col-md-1">
                <label>التاريخ</label>
            </div>
        </div>
        <br />
        <div class="row text-center" style="margin: auto;">
            <div class="col-md-2">
                <asp:Label runat="server" ID="idlbl"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" ID="namelbl"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" ID="emaillbl"></asp:Label>
            </div>
            <div class="col-md-5">
                <asp:Label runat="server" ID="contentlbl"></asp:Label>
            </div>
            <div class="col-md-1">
                <asp:Label runat="server" ID="datelbl"></asp:Label>
                <asp:Label runat="server" ID="isreadlbl" Visible="false"></asp:Label>
            </div>
        </div>
    </div>
    <br />
    <br />

</asp:Content>

