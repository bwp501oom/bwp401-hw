﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;
using System.Text.RegularExpressions;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["username"] != null)
        {
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
            {

                string query = "SELECT is_admin from Accounts WHERE username = @username";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@username", Session["username"].ToString());
                con.Open();
                string admin = Convert.ToString(cmd.ExecuteScalar());
                if (admin == "True")
                {
                    AddProductDiv.Visible = true;
                }
                con.Close();
            }
        }
        if (!IsPostBack)
        {
            string query;
            string SearchThis = SearchBox.Text;
            if (SearchBox.Text.Length >= 1)
            {
                query = "SELECT * FROM Products p WHERE p.product_name LIKE '%" + SearchThis + "%' OR p.product_type LIKE '%" + SearchThis + "s%' OR p.product_brand LIKE '%" + SearchThis + "%' OR p.product_year LIKE '%" + SearchThis + "%' OR p.pc_storage LIKE '%" + SearchThis + "%' OR p.pc_ram LIKE '%" + SearchThis + "%' OR p.pc_processor LIKE '%" + SearchThis + "%' OR p.pc_resolution LIKE '%" + SearchThis + "%' OR p.pc_model LIKE '%" + SearchThis + "%' OR p.pc_gpu LIKE '%" + SearchThis + "%' OR p.printer_resolution LIKE '%" + SearchThis + "%' OR p.printer_speed LIKE '%" + SearchThis + "%' OR p.printer_paper LIKE '%" + SearchThis + "%' OR p.printer_type LIKE '%" + SearchThis + "%' OR p.printer_memory LIKE '%" + SearchThis + "%' OR p.scanner_type LIKE '%" + SearchThis + "%' OR p.scanner_speed LIKE '%" + SearchThis + "%' OR p.scanner_paper LIKE '%" + SearchThis + "%' OR p.scanner_resolution LIKE '%" + SearchThis + "%' OR p.price LIKE '%" + SearchThis + "%' OR p.color LIKE '%" + SearchThis + "%'";
                BindRepeator();
            }

            else
            {
                query = "SELECT * FROM Products";
                BindRepeator();
            }
            
        }
        
        
    }
    protected void NewProduct_Click(object sender, EventArgs e)
    {
        Response.Redirect("new_product.aspx");
    }
    private void BindRepeator()
    {
        using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
        {
            string query;
            string SearchThis = SearchBox.Text;
            if (SearchBox.Text.Length >= 1)
            {
                query = "SELECT * FROM Products p WHERE p.product_name LIKE '%" + SearchThis + "%' OR p.product_type LIKE '%" + SearchThis + "s%' OR p.product_brand LIKE '%" + SearchThis + "%' OR p.product_year LIKE '%" + SearchThis + "%' OR p.pc_storage LIKE '%" + SearchThis + "%' OR p.pc_ram LIKE '%" + SearchThis + "%' OR p.pc_processor LIKE '%" + SearchThis + "%' OR p.pc_resolution LIKE '%" + SearchThis + "%' OR p.pc_model LIKE '%" + SearchThis + "%' OR p.pc_gpu LIKE '%" + SearchThis + "%' OR p.printer_resolution LIKE '%" + SearchThis + "%' OR p.printer_speed LIKE '%" + SearchThis + "%' OR p.printer_paper LIKE '%" + SearchThis + "%' OR p.printer_type LIKE '%" + SearchThis + "%' OR p.printer_memory LIKE '%" + SearchThis + "%' OR p.scanner_type LIKE '%" + SearchThis + "%' OR p.scanner_speed LIKE '%" + SearchThis + "%' OR p.scanner_paper LIKE '%" + SearchThis + "%' OR p.scanner_resolution LIKE '%" + SearchThis + "%' OR p.price LIKE '%" + SearchThis + "%' OR p.color LIKE '%" + SearchThis + "%'";
            }

            else
            {
                query = "SELECT * FROM Products";
            }
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            con.Open();
            DataTable dt = new DataTable();
            sda.Fill(dt);
            int price = 0;
            int year = 0;
            string name = "";
            string type = "";
            foreach (DataRow row in dt.Rows)
            {
                price = Convert.ToInt32(Regex.Match(row["price"].ToString(), @"\d+").Value);
                year = Convert.ToInt32(Regex.Match(row["product_year"].ToString(), @"\d+").Value);
                name = Convert.ToString(row["product_name"]);
                type = Convert.ToString(row["product_type"]);
                if (ProductNameTB.Text.Length > 0 && !name.Contains(ProductNameTB.Text))
                {
                    row.Delete();
                }
                if (ProductPriceMinTB.Text.Length > 0 && price < Convert.ToInt32(ProductPriceMinTB.Text))
                {
                    row.Delete();
                }
                if (ProductPriceMaxTB.Text.Length > 0 && price > Convert.ToInt32(ProductPriceMaxTB.Text))
                {
                    row.Delete();
                }
                if (ProductYearMinTB.Text.Length > 0 && year < Convert.ToInt32(ProductYearMinTB.Text))
                {
                    row.Delete();
                }
                if (ProductYearMaxTB.Text.Length > 0 && year > Convert.ToInt32(ProductYearMaxTB.Text))
                {
                    row.Delete();
                }
                if (ProductTypeDDL.SelectedValue.Length > 0 && !type.Contains(ProductTypeDDL.SelectedValue))
                {
                    row.Delete();
                }

            }
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
            if (Repeater1.Items.Count == 0)
            {
                validator.Text = "لم يتم ايجاد أي منتج.";
            }
            else
            {
                validator.Text = "";
            }
            
        }
    }
    protected void DeleteProductNameTB_Click(object sender, EventArgs e)
    {
        ProductNameTB.Text = "";
        FilterValidator.Text = "";
        ProductNameDiv.Visible = false;
        BindRepeator();
    }
    protected void DeleteProductPriceMinTB_Click(object sender, EventArgs e)
    {
        ProductPriceMinTB.Text = "";
        ProductPriceMaxTB.Text = "";
        FilterValidator.Text = "";
        ProductPriceDiv.Visible = false;
        BindRepeator();
    }
    protected void DeleteProductTypeDDL_Click(object sender, EventArgs e)
    {
        ProductTypeDDL.SelectedIndex = 0;
        FilterValidator.Text = "";
        ProductTypeDiv.Visible = false;
        BindRepeator();
    }
    protected void DeleteYearDivBtn_Click(object sender, EventArgs e)
    {
        ProductYearMinTB.Text = "";
        ProductYearMaxTB.Text = "";
        FilterValidator.Text = "";
        ProductYearDiv.Visible = false;
        BindRepeator();
    }
    protected void SearchBtn_Click(object sender, EventArgs e)
    {
        bool Valid = Validator();
        if (Valid == true)
        {
            BindRepeator();
        }
        else
        {
            FilterValidator.Text = "الرجاء استخدام الارقام فقط للبحث عن سعر المنتج او سنة الانتاج.";
            Valid = false;
        }
    }
    bool Validator()
    {
        bool myValidator = true;
        if (ProductPriceMinTB.Text.Length > 0 && IsDigitsOnly(ProductPriceMinTB.Text) == false || ProductPriceMinTB.Text.Length > 10)
        {
            myValidator = false;
            BoxValidator(ProductPriceMinTB, 1);
        }
        else
        {
            BoxValidator(ProductPriceMinTB, 2);
        }
        if (ProductPriceMaxTB.Text.Length > 0 && IsDigitsOnly(ProductPriceMaxTB.Text) == false || ProductPriceMaxTB.Text.Length > 10)
        {
            myValidator = false;
            BoxValidator(ProductPriceMaxTB, 1);
        }
        else
        {
            BoxValidator(ProductPriceMaxTB, 2);
        }
        if (ProductYearMinTB.Text.Length > 0 && IsDigitsOnly(ProductYearMinTB.Text) == false || ProductYearMinTB.Text.Length > 4)
        {
            myValidator = false;
            BoxValidator(ProductYearMinTB, 1);
        }
        else
        {
            BoxValidator(ProductYearMinTB, 2);
        }
        if (ProductYearMaxTB.Text.Length > 0 && IsDigitsOnly(ProductYearMaxTB.Text) == false || ProductYearMaxTB.Text.Length > 4)
        {
            myValidator = false;
            BoxValidator(ProductYearMaxTB, 1);
        }
        else
        {
            BoxValidator(ProductYearMaxTB, 2);
        }
        if (myValidator == true)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    protected void BoxValidator(TextBox textboxName, int num)
    {
        if (num == 1)
        {
            textboxName.CssClass = "box-edited danger";
        }
        else
        {
            textboxName.CssClass = "box-edited";
        }

    }
    bool IsDigitsOnly(string str)
    {
        foreach (char c in str)
        {
            if (c < '0' || c > '9')
                return false;
        }

        return true;
    }


    protected void FilterDDL_SelectedIndexChanged(object sender, EventArgs e)
    {
        string filter = FilterDDL.SelectedValue;
        bool filtered = false;
        if (FilterDDL.SelectedValue == "productName")
        {
            if (ProductNameDiv.Visible == true)
            {
                filtered = true;
            }
            else
            {
                ProductNameDiv.Visible = true;
            }
        }
        else if (FilterDDL.SelectedValue == "productPrice")
        {
            if (ProductPriceDiv.Visible == true)
            {
                filtered = true;
            }
            else
            {
                ProductPriceDiv.Visible = true;
            }
        }
        else if (FilterDDL.SelectedValue == "productYear")
        {
            if (ProductYearDiv.Visible == true)
            {
                filtered = true;
            }
            else
            {
                ProductYearDiv.Visible = true;
            }
        }
        else if (FilterDDL.SelectedValue == "productType")
        {
            if (ProductTypeDiv.Visible == true)
            {
                filtered = true;
            }
            else
            {
                ProductTypeDiv.Visible = true;
            }
        }
        if (filtered == true)
        {
            FilterValidator.Text = "لقد تم اضافة الفلتر سابقا";
        }
        else
        {
            FilterValidator.Text = "";
        }
    }
    protected void SearchBox_TextChanged(object sender, EventArgs e)
    {
        string query;
        string SearchThis = SearchBox.Text;
        if (SearchBox.Text.Length >= 1)
        {
            query = "SELECT * FROM Products p WHERE p.product_name LIKE '%" + SearchThis + "%' OR p.product_type LIKE '%" + SearchThis + "s%' OR p.product_brand LIKE '%" + SearchThis + "%' OR p.product_year LIKE '%" + SearchThis + "%' OR p.pc_storage LIKE '%" + SearchThis + "%' OR p.pc_ram LIKE '%" + SearchThis + "%' OR p.pc_processor LIKE '%" + SearchThis + "%' OR p.pc_resolution LIKE '%" + SearchThis + "%' OR p.pc_model LIKE '%" + SearchThis + "%' OR p.pc_gpu LIKE '%" + SearchThis + "%' OR p.printer_resolution LIKE '%" + SearchThis + "%' OR p.printer_speed LIKE '%" + SearchThis + "%' OR p.printer_paper LIKE '%" + SearchThis + "%' OR p.printer_type LIKE '%" + SearchThis + "%' OR p.printer_memory LIKE '%" + SearchThis + "%' OR p.scanner_type LIKE '%" + SearchThis + "%' OR p.scanner_speed LIKE '%" + SearchThis + "%' OR p.scanner_paper LIKE '%" + SearchThis + "%' OR p.scanner_resolution LIKE '%" + SearchThis + "%' OR p.price LIKE '%" + SearchThis + "%' OR p.color LIKE '%" + SearchThis + "%'";
            BindRepeator();
        }
        else
        {
            query = "SELECT * FROM Products";
            BindRepeator();
        }
    }
    string GetQuery()
    {
        string query;
        string SearchThis = SearchBox.Text;
        if (SearchBox.Text.Length >= 1)
        {
            query = "SELECT * FROM Products p WHERE p.product_name LIKE '%" + SearchThis + "%' OR p.product_type LIKE '%" + SearchThis + "s%' OR p.product_brand LIKE '%" + SearchThis + "%' OR p.product_year LIKE '%" + SearchThis + "%' OR p.pc_storage LIKE '%" + SearchThis + "%' OR p.pc_ram LIKE '%" + SearchThis + "%' OR p.pc_processor LIKE '%" + SearchThis + "%' OR p.pc_resolution LIKE '%" + SearchThis + "%' OR p.pc_model LIKE '%" + SearchThis + "%' OR p.pc_gpu LIKE '%" + SearchThis + "%' OR p.printer_resolution LIKE '%" + SearchThis + "%' OR p.printer_speed LIKE '%" + SearchThis + "%' OR p.printer_paper LIKE '%" + SearchThis + "%' OR p.printer_type LIKE '%" + SearchThis + "%' OR p.printer_memory LIKE '%" + SearchThis + "%' OR p.scanner_type LIKE '%" + SearchThis + "%' OR p.scanner_speed LIKE '%" + SearchThis + "%' OR p.scanner_paper LIKE '%" + SearchThis + "%' OR p.scanner_resolution LIKE '%" + SearchThis + "%' OR p.price LIKE '%" + SearchThis + "%' OR p.color LIKE '%" + SearchThis + "%'";
            return query;
        }
        if (SearchBox.Text.Length == 0)
        {
            query = "SELECT * FROM Products";
            return query;
        }
        else
        {
            query = "SELECT * FROM Products";
            return query;
        }
    }
}