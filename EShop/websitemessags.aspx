﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="websitemessags.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>رسائل الموقع&nbsp;|&nbsp;EShop</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <div class="main inside-main" style="direction: rtl;">
        <br />
        <br />
        <div class="row">
            <div class="col-md-12" style="text-align: center; font-size: 30px;">
                <label>رسائل المستخدمين</label>
            </div>
        </div>
        <div class="row text-center" style="margin: auto; background-color: #232F3E; color: white; font-size: 24px;">
            <div class="col-md-2">
                <label>رقم الرسالة</label>
            </div>
            <div class="col-md-2">
                <label>اسم الحساب</label>
            </div>
            <div class="col-md-2">
                <label>الايميل</label>
            </div>
            <div class="col-md-5">
                <label>الرسالة</label>
            </div>
            <div class="col-md-1">
                <label>التاريخ</label>
            </div>
        </div>
        <asp:Repeater runat="server" ID="MessagesTable">
            <ItemTemplate>
                <div class="row  text-center" style="margin: auto;">
                    <div class="col-md-2">
                        <asp:Label runat="server" Text='<%#Eval("id")%>'></asp:Label></td>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("name")%>'></asp:Label></td>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("email")%>'></asp:Label>
                    </div>
                    <div class="col-md-5" style="margin: auto;">
                        <a runat="server" href='<%# Eval("id", "~/messagedetails.aspx?Id={0}") %>'>
                            <asp:Label runat="server" Text='<%#Eval("content").ToString().Substring(0,20)+"..."%>'></asp:Label>
                        </a>
                    </div>
                    <div class="col-md-1" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("date")%>'></asp:Label>
                        <asp:Label runat="server" Text='<%#Eval("is_read")%>' Visible="false"></asp:Label>
                    </div>
                </div>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <div class="row  text-center" style="margin: auto; background-color: #EFF0F1;">
                    <div class="col-md-2">
                        <asp:Label runat="server" Text='<%#Eval("id")%>'></asp:Label></td>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("name")%>'></asp:Label></td>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("email")%>'></asp:Label>
                    </div>
                    <div class="col-md-5" style="margin: auto;">
                        <a runat="server" href='<%# Eval("id", "~/messagedetails.aspx?Id={0}") %>'>
                            <asp:Label runat="server" Text='<%#Eval("content").ToString().Substring(0,20)+"..."%>'></asp:Label>
                        </a>
                    </div>
                    <div class="col-md-1" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("date")%>'></asp:Label>
                        <asp:Label runat="server" Text='<%#Eval("is_read")%>' Visible="false"></asp:Label>
                    </div>
                </div>
            </AlternatingItemTemplate>

        </asp:Repeater>

        <br />
        <br />
    </div>
    <asp:Label runat="server" ID="x"></asp:Label>
</asp:Content>

