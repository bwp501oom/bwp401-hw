﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Text.RegularExpressions;

public partial class _Default : System.Web.UI.Page
{
    int total_price = 0;
    int MaxId = 0;
    string ProductList = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        BindRepeator();
        if (Session["username"] == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
            if (!this.IsPostBack)
            {
                
                BindRepeator2();
            }
        }
    }
    protected void DeleteProduct_Click(object sender, EventArgs e)
    {
        int productID = int.Parse(((sender as LinkButton).NamingContainer.FindControl("ProductId") as Label).Text);
        using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("DELETE FROM Order_Item WHERE Id = @Id", con))
            {
                cmd.Parameters.AddWithValue("@Id", productID);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        this.BindRepeator();
    }

    private void BindRepeator()
    {

        try
        {
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
            {
                string query = "SELECT * FROM Order_Item WHERE username_id = '" + Session["username"].ToString() + "'";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    ProductList += row["product_name"].ToString() + "," + row["quantity"].ToString() + "-";
                    int price = Convert.ToInt32(Regex.Match(row["total_price"].ToString(), @"\d+").Value);
                    total_price += price;

                }

                ProductHolderLabel.Text = ProductList;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                ItemCount.Text = dt.Rows.Count.ToString();
                if (dt.Rows.Count == 0)
                {
                    TopHeader.Visible = false;
                    lbl1.Text = "لا يوجد منتجات حاليا في سلتك.";
                    NoProductsFound.Visible = true;
                    TotalPriceDivOrder.Visible = false;
                }
                else
                {
                    ProductList = ProductList.Remove(ProductList.Length - 1, 1);
                }
                TotalPrice.Text = total_price.ToString() + " ليرة سورية";
                string query2 = "SELECT MAX(order_id) FROM Order_Placed";
                SqlCommand cmd2 = new SqlCommand(query2, con);
                con.Open();
                MaxId = Convert.ToInt32(cmd2.ExecuteScalar());
                MaxId += 1;
                MaxCounter.Text = MaxId.ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void SubmitOrder_Click(object sender, EventArgs e)
    {
        String Date = DateTime.Now.ToString();
        
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
            {
                string query = "INSERT INTO Order_Placed (username_id,product_id,quantity,total_price,product_image,product_name,order_id) SELECT username_id,product_id,quantity,total_price,product_image,product_name,(@order_id) FROM Order_Item WHERE username_id = @username_id";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.Parameters.AddWithValue("@order_id", MaxCounter.Text);
                cmd.Parameters.AddWithValue("@username_id", Session["username"].ToString());
                int i = cmd.ExecuteNonQuery();
                if (i != 0)
                {
                    testlabel.Text = "تم اضافة الطلب";
                    string query2 = "SELECT address FROM Accounts WHERE username = @username";
                    SqlCommand cmd2 = new SqlCommand(query2, con);
                    cmd2.Parameters.AddWithValue("@username", Session["username"].ToString());
                    string address = Convert.ToString(cmd2.ExecuteScalar());
                    if (address.Length > 0)
                    {
                        string query3 = "INSERT INTO Orders (order_id, order_price, order_address, username, status, date_ordered) VALUES (@order_id, @order_price, @order_address, @username, @status, @date_ordered)";
                        SqlCommand cmd3 = new SqlCommand(query3, con);
                        cmd3.Parameters.AddWithValue("@order_id", MaxCounter.Text);
                        cmd3.Parameters.AddWithValue("@order_price", TotalPrice.Text);
                        cmd3.Parameters.AddWithValue("@order_address", address);
                        cmd3.Parameters.AddWithValue("@username", Session["username"].ToString());
                        cmd3.Parameters.AddWithValue("@status", "Ongoing");
                        cmd3.Parameters.AddWithValue("@date_ordered", Date);
                        int k = cmd3.ExecuteNonQuery();
                        if (k != 0)
                        {
                            string query4 = "DELETE FROM Order_Item WHERE username_id = @username_id";
                            SqlCommand cmd4 = new SqlCommand(query4, con);
                            cmd4.Parameters.AddWithValue("@username_id", Session["username"].ToString());
                            cmd4.ExecuteNonQuery();
                            PastOrdersDiv.Visible = true;



                            string query5 = "SELECT quantity, total_sales FROM Sales WHERE product_name = @product_name";
                            SqlCommand cmd5 = new SqlCommand(query5, con);
                            int ProductQuantity;
                            int currentQuantity = 0;
                            int ProductTotalSales = 0;
                            string query6 = "UPDATE Sales SET quantity = @quantity, total_sales = @total_sales WHERE product_name = @product_name";
                            SqlCommand cmd6 = new SqlCommand(query6, con);
                            SqlDataAdapter sda = new SqlDataAdapter();
                            var words = ProductHolderLabel.Text.Split(@"-".ToCharArray());
                            for (int a = 0; a < Convert.ToInt32(ItemCount.Text); a++)
                            {
                                var salesBoth = words[a].Split(@",".ToCharArray());
                                cmd5.Parameters.AddWithValue("@product_name", salesBoth[0]);
                                using (SqlDataReader sdr = cmd5.ExecuteReader())
                                {
                                    sdr.Read();
                                    currentQuantity = Convert.ToInt32(sdr["quantity"]);
                                    ProductTotalSales = Convert.ToInt32(sdr["total_sales"]);
                                }
                                ProductQuantity = currentQuantity + Convert.ToInt32(salesBoth[1]);
                                cmd6.Parameters.AddWithValue("@quantity", ProductQuantity);
                                cmd6.Parameters.AddWithValue("@product_name", salesBoth[0]);
                                cmd6.Parameters.AddWithValue("@total_sales", ProductTotalSales * ProductQuantity);
                                sda.UpdateCommand = cmd6;
                                sda.UpdateCommand.ExecuteNonQuery();
                                cmd5.Parameters.Clear();
                                cmd6.Parameters.Clear();
                            }



                            BindRepeator2();
                        }
                    }
                    
                }
                con.Close();
            }
            BindRepeator();

    }
    protected void DeleteOrder_Click(object sender, EventArgs e)
    {
        int orderID = int.Parse(((sender as LinkButton).NamingContainer.FindControl("OrderID") as Label).Text);
        using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("DELETE FROM Orders WHERE order_id = @order_id", con))
            {
                cmd.Parameters.AddWithValue("@order_id", orderID);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        this.BindRepeator2();
    }
    private void BindRepeator2()
    {

        try
        {
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
            {
                string query = "SELECT * FROM Orders WHERE username = '" + Session["username"].ToString() + "'";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt.Rows.Count == 0)
                {
                    PastOrdersDiv.Visible = false;
                }
                Repeater2.DataSource = dt;
                Repeater2.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }
    public static string[] mySplit(string source, params string[] delimiters)
    {
        return source.Split(delimiters, StringSplitOptions.None);
    }
    
}