﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;


public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void SendMessage_Click(object sender, EventArgs e)
    {
        bool validator = true;
        if (NameTB.Text == "")
        {
            BoxValidator(NameTB, 1);
            validator = false;
        }
        else
        {
            BoxValidator(NameTB, 2);
        }
        if (!EmailTB.Text.Contains("@") && !EmailTB.Text.Contains("."))
        {
            BoxValidator(EmailTB, 1);
            validator = false;
            MessageLB.Text = "الرجاء التاكد من ادخال المعلومات المطلوبة ";
        }
        else
        {
            BoxValidator(EmailTB, 2);

        }
        if (ContentTA.Text == "")
        {
            BoxValidator(ContentTA, 1);
            validator = false;
        }
        else
        {
            BoxValidator(ContentTA, 2);
        }

        if (validator == false)
        {
            MessageLB.Text = "الرجاء التحقق من ادخال المعلومات المطلوبة";
        }
        else
        {
            try
            {
                using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
                {

                    string query = "INSERT INTO Messages (name, email, content,date) VALUES (@username, @email, @content, @date)";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@username", NameTB.Text);
                    cmd.Parameters.AddWithValue("@email", EmailTB.Text);
                    cmd.Parameters.AddWithValue("@content", ContentTA.Text);
                    cmd.Parameters.AddWithValue("@date", DateTime.Now);
                    con.Open();
                    int i = cmd.ExecuteNonQuery();
                    if (i != 0)
                    {
                        MessageLB.Text = "سنرد على رسالتك في اقرب فرصة";
                    }
                    con.Close();
                    NameTB.Text = "";
                    EmailTB.Text = "";
                    ContentTA.Text = "";
                }
            }
            catch
            {
                MessageLB.Text = "الرجاء التاكد من ادخال المعلومات المطلوبة ";
            }
        }

    }
    protected void BoxValidator(TextBox textboxName, int num)
    {
        if (num == 1)
        {
            textboxName.CssClass = "input danger";
        }
        else
        {
            textboxName.CssClass = "input";
        }

    }


}
