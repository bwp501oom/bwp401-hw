﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="accounts.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>الحسابات&nbsp;|&nbsp;EShop</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <div class="main inside-main accounts-list" style="direction: rtl; text-align: center; font-size: 18px;">
        <br />
        <div class="row" style="background-color: #232F3E; margin-right: 10px; margin-left: 10px; color:white;">
            <div class="col-md-1">
                <asp:Label runat="server" Text="الرقم"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="الاسم"></asp:Label>
            </div>
            <div class="col-md-3">
                <asp:Label runat="server" Text="الايميل"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="كلمة المرور"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="العنوان"></asp:Label>
            </div>
            <div class="col-md-1">
                <asp:Label runat="server" Text="رقم الهاتف"></asp:Label>
            </div>
            <div class="col-md-1">
                <asp:Label runat="server" Text="مدير"></asp:Label>
            </div>
        </div>
        <asp:Repeater runat="server" ID="Repeater1">
            <AlternatingItemTemplate>
                <a runat="server" href='<%# Eval("id", "~/account.aspx?Id={0}&is_admin=yes") %>' style="text-decoration: none;">
                    <div class="row" style="background-color: white; margin-right: 10px; margin-left: 10px; text-align: center;">
                        <div class="col-md-1">
                            <asp:Label runat="server" Text='<%#Eval("Id")%>'></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <asp:Label runat="server" Text='<%#Eval("username")%>'></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:Label runat="server" Text='<%#Eval("email")%>'></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <asp:Label runat="server" Text='<%#Eval("password")%>'></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <asp:Label runat="server" Text='<%#Eval("address")%>'></asp:Label>
                        </div>
                        <div class="col-md-1">
                            <asp:Label runat="server" Text='<%#Eval("mobile_number")%>'></asp:Label>
                        </div>
                        <div class="col-md-1">
                            <asp:Label runat="server" Text='<%#Eval("is_admin")%>'></asp:Label>
                        </div>
                    </div>
                </a>

            </AlternatingItemTemplate>
            <ItemTemplate>
                <a runat="server" href='<%# Eval("id", "~/account.aspx?Id={0}&is_admin=yes") %>' style="text-decoration: none;">
                    <div class="row" style="background-color: #EFF0F1; margin-right: 10px; margin-left: 10px; text-align: center;">
                        <div class="col-md-1">
                            <asp:Label runat="server" Text='<%#Eval("Id")%>'></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <asp:Label runat="server" Text='<%#Eval("username")%>'></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:Label runat="server" Text='<%#Eval("email")%>'></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <asp:Label runat="server" Text='<%#Eval("password")%>'></asp:Label>
                        </div>
                        <div class="col-md-2">
                            <asp:Label runat="server" Text='<%#Eval("address")%>'></asp:Label>
                        </div>
                        <div class="col-md-1">
                            <asp:Label runat="server" Text='<%#Eval("mobile_number")%>'></asp:Label>
                        </div>
                        <div class="col-md-1">
                            <asp:Label runat="server" Text='<%#Eval("is_admin")%>'></asp:Label>
                        </div>
                    </div>
                </a>
            </ItemTemplate>
        </asp:Repeater>
        <br />
        <br />
    </div>
</asp:Content>

