﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] != null)
        {
            string cond = String.Format("id={0}", Convert.ToString(Session["user_id"]));
            Account cur_user = Account.filter(cond)[0];
            if (cur_user.is_admin)
            {
                if (IsPostBack)
                {
                    BindRepeator("");
                    //if (namefl.Value == "" && emailfl.Value == "" && contentfl.Value == "" && readF.Value == "ch" && readT.Value == "ch")
                    //{
                        
                    //}
                    //else
                    //{
                    //    string sqlwhere = "WHERE ";
                    //    sqlwhere += (namefl.Value != "") ? String.Format("name LIKE '%{0}%'", namefl.Value) : "";
                    //    sqlwhere += (namefl.Value != "" && (emailfl.Value != "" || contentfl.Value != "" /*|| readF.Value == "ch" || readT.Value == "ch"*/)) ? " AND " : "";
                    //    sqlwhere += (emailfl.Value != "") ? String.Format("email LIKE '%{0}%'  ", emailfl.Value) : "";
                    //    sqlwhere += (emailfl.Value != "" && (contentfl.Value != "" /*|| readF.Value == "ch" || readT.Value == "ch"*/)) ? " AND " : "";
                    //    sqlwhere += (contentfl.Value != "") ? String.Format("content LIKE '%{0}%'", contentfl.Value) : "";
                    //    //sqlwhere += (contentfl.Value != "" && ( readF.Value == "ch" || readT.Value == "ch")) ? " AND " : "";
                    //    //sqlwhere += (readT.Value == "ch") ? "is_read='True'" : "";
                    //    //sqlwhere += (readF.Value == "ch" && readT.Value == "ch") ? " AND " : "";
                    //    //sqlwhere += (readF.Value == "ch") ? "is_read='False'" : "";
                    //    x.Text = sqlwhere;
                    //    BindRepeator(sqlwhere);


                    //}

                }
                else
                {
                    //readF.Checked = true;
                    BindRepeator("WHERE is_read='False'");
                }
            }

            else
            {
                Response.Redirect("index.aspx");
            }
        }
        else
        {
            Response.Redirect("index.aspx");
        }


    }

    private void BindRepeator(string sqlwhere)
    {
        using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
        {
            string query = "SELECT * FROM messages " + sqlwhere;
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();

            MessagesTable.DataSource = cmd.ExecuteReader();
            MessagesTable.DataBind();
            con.Close();
        }
    }


}