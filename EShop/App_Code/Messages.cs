﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

/// <summary>
/// Summary description for messages
/// </summary>
public class messages
{
    public static SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString);

    public int id = 0;
    public string name = "";
    public string email = "";
    public string content = "";
    public DateTime date = DateTime.Now;
    public bool is_read = false;
    public messages(int id = 0, string name = "", string email = "", string content = "", string date = "06/06/2020 11:20:24 PM", bool is_read = false)
    {
        this.id = id;
        this.name = name;
        this.email = email;
        this.content = content;
        this.date = DateTime.Parse(date);
        this.is_read = is_read;
    }
    public static List<messages> filter(string sqlwhere)
    {

        string query = "SELECT * FROM messages " + sqlwhere;
        SqlCommand cmd = new SqlCommand(query, con);
        if (messages.con.State == ConnectionState.Closed)
            con.Open();
        SqlDataReader DReader = cmd.ExecuteReader();
        List<messages> result = new List<messages>();

        while (DReader.Read())
        {
            int id = DReader.GetInt32(DReader.GetOrdinal("id"));
            string name = DReader.GetString(DReader.GetOrdinal("name"));
            string email = DReader.GetString(DReader.GetOrdinal("email"));
            string content = DReader.GetString(DReader.GetOrdinal("content"));
            string date = DReader.GetDateTime(DReader.GetOrdinal("date")).ToString();
            bool is_read = DReader.GetBoolean(DReader.GetOrdinal("is_read"));

            messages mess = new messages(id: id, name: name, email: email, content: content, date: date, is_read: is_read);

            result.Add(mess);
        }
        if (messages.con.State == ConnectionState.Open)
            messages.con.Close();


        return result;
    }
}