﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
/// <summary>
/// Summary description for Class1
/// </summary>
public class Account
{
    public int id = 0;
    public string username = "";
    public string email = "";
    public string password = "";
    public string mobile_number = "";
    public bool is_admin = false;
    public string address = "";
    public static SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString);
    public Account(string username = "", string email = "", string password = "", bool is_admin = false, string mobile_number = "", string address = "")
    {

        this.username = username;
        this.email = email;
        this.mobile_number = mobile_number;
        this.password = password;
        this.address = address;
        this.is_admin = is_admin;
    }
    public bool save()
    {
        if (!is_authernicated())
        {
            string query = "INSERT INTO Accounts (username, email, password, address, mobile_number, is_admin) VALUES (@username, @email, @password, @address, @mobile_number, @is_admin)";
            SqlCommand cmd = new SqlCommand(query, Account.con);
            cmd.Parameters.AddWithValue("@username", this.username);
            cmd.Parameters.AddWithValue("@email", this.email);
            cmd.Parameters.AddWithValue("@password", this.password);
            cmd.Parameters.AddWithValue("@address", this.address);
            cmd.Parameters.AddWithValue("@mobile_number", this.mobile_number);
            cmd.Parameters.AddWithValue("@is_admin", (this.is_admin));
            if (Account.con.State == ConnectionState.Closed) con.Open();
            int i = cmd.ExecuteNonQuery();
            if (Account.con.State == ConnectionState.Open) Account.con.Close();
            if (i != 0) { return true; }
        }
        return false;

    }
    public bool is_authernicated()
    {
        if (this.email.Length > 0 && this.password.Length > 0)
        {
            if (Account.con.State == ConnectionState.Closed) con.Open();
            string query = "SELECT id FROM Accounts WHERE email = @email AND password = @password";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@password", password);
            int id = Convert.ToInt32(cmd.ExecuteScalar());
            if (Account.con.State == ConnectionState.Open) Account.con.Close();
            if (id > 0)
            {
                return true;
            }
        }

        return false;
    }

    public static Account authernicate(string email, string password)
    {
        string sqlwhere = String.Format("email= '{0}' AND password= '{1}' ", email, password);
        //sqlwhere = "email= 'omar@gmail.com' AND password= '12345' ";
        //"SELECT * FROM Accounts WHERE email= 'omar@gmail.com' AND password= '12344'"
        if (filter(sqlwhere)[0].id > 0)
        {
            return filter(sqlwhere)[0];
        }
        Account result = filter(sqlwhere)[0];
        return result;
    }

    public static Account authernicateById(int id)
    {
        string sqlwhere = String.Format("id= {0} ", id);
        //sqlwhere = "email= 'omar@gmail.com' AND password= '12345' ";
        //"SELECT * FROM Accounts WHERE email= 'omar@gmail.com' AND password= '12344'"
        if (filter(sqlwhere)[0].id > 0)
        {
            return filter(sqlwhere)[0];
        }
        Account result = filter(sqlwhere)[0];
        return result;
    }

    public static List<Account> filter(string sqlwhere)
    {
        if (Account.con.State == ConnectionState.Closed) con.Open();
        string query = "SELECT * FROM Accounts WHERE " + sqlwhere;
        SqlCommand cmd = new SqlCommand(query, con);
        SqlDataReader DReader = cmd.ExecuteReader();
        List<Account> result = new List<Account>();

        while (DReader.Read())
        {
            int id = DReader.GetInt32(DReader.GetOrdinal("id"));
            string username = DReader.GetString(DReader.GetOrdinal("username"));
            string email = DReader.GetString(DReader.GetOrdinal("email"));
            string password = DReader.GetString(DReader.GetOrdinal("password"));
            string mobile_number = DReader.GetString(DReader.GetOrdinal("mobile_number"));
            bool is_admin = DReader.GetBoolean(DReader.GetOrdinal("is_admin"));
            string address = DReader.GetString(DReader.GetOrdinal("address"));
            Account acc = new Account(username, email, password, is_admin, mobile_number, address);
            acc.id = id;
            result.Add(acc);
        }
        if (Account.con.State == ConnectionState.Open) Account.con.Close();
        return result;
    }

}