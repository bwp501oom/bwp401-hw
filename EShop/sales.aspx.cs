﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Text.RegularExpressions;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] != null)
        {

            string AccountName = Session["username"].ToString();


            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
            {
                con.Open();
                string query = "SELECT Id FROM Accounts WHERE username = @username ";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@username", Session["username"].ToString());
                string id = Convert.ToString(cmd.ExecuteScalar());
                if (id.Length > 0)
                {
                    
                    string query2 = "SELECT is_admin FROM Accounts WHERE Id = @Id";
                    SqlCommand cmd2 = new SqlCommand(query2, con);
                    cmd2.Parameters.AddWithValue("@Id", id);
                    string admin = Convert.ToString(cmd2.ExecuteScalar());
                    
                    if (admin == "True")
                    {
                        BindRepeator();
                        BindRepeator2();
                    }
                    else
                    {
                        Response.Redirect("index.aspx");
                    }
                }
                else
                {
                    
                }
                con.Close();
            }
        }
        else
        {
            Response.Redirect("index.aspx");
        }
    }
    private void BindRepeator()
    {
        Int64 total_price = 0;
        using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
        {
            string query = "SELECT * FROM Sales";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            Repeater1.DataSource = dt;
            gridview1.DataSource = dt;
            string[] delete0 = { "quantity" };
            foreach (DataRow row in dt.Rows)
            {
                int quantity = Convert.ToInt32(Regex.Match(row["quantity"].ToString(), @"\d+").Value);
                if (quantity <= 0)
                {
                    row.Delete();
                }
                else
                {
                    int price = Convert.ToInt32(Regex.Match(row["total_sales"].ToString(), @"\d+").Value);
                    total_price += price;
                }
            }
            Repeater1.DataBind();
            string[] delete = { "image" };
            foreach (string ColName in delete)
            {
                if (dt.Columns.Contains(ColName))
                    dt.Columns.Remove(ColName);

            }
            gridview1.DataBind();
            total.Text = total_price.ToString();
        }
    }

    protected void DeleteOrder_Click(object sender, EventArgs e)
    {
        int orderID = int.Parse(((sender as LinkButton).NamingContainer.FindControl("OrderID") as Label).Text);
        using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("DELETE FROM Orders WHERE order_id = @order_id", con))
            {
                cmd.Parameters.AddWithValue("@order_id", orderID);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        this.BindRepeator2();
    }
    private void BindRepeator2(string date=null, int mode = 0)
    {
        String sDate = DateTime.Now.ToString();
        DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

        String day = datevalue.Day.ToString();
        String month = datevalue.Month.ToString();
        String year = datevalue.Year.ToString();
        try
        {
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
            {
                string query = "SELECT * FROM Orders";
                if (date != null)
                {
                    if (mode == 1)
                    {
                        int enteredDay = Convert.ToInt32(date);
                        query = "SELECT * FROM Orders WHERE date_ordered BETWEEN '" + year + "-" + month + "-" + (enteredDay - 7) + "' AND  '" + year + "-" + month + "-" + (enteredDay +1) + "'";
                        TestLabel.Text = query;
                    }
                    if (mode == 2)
                    {
                        int enteredMonth = Convert.ToInt32(date);
                        query = "SELECT * FROM Orders WHERE date_ordered BETWEEN '" + year + "-" + (enteredMonth - 1) + "-" + day + "' AND  '" + year + "-" + (enteredMonth +1) + "-" + day + "'";

                    }
                    if (mode == 3)
                    {
                        TestingDate.Text = FromDate.Text;
                        string fromdate = FromDate.Text;
                        string todate = ToDate.Text;
                        query = "SELECT * FROM Orders WHERE date_ordered BETWEEN '" + fromdate + "' AND  '" + todate + "'";

                    }
                    
                }
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {

                }
                Repeater2.DataSource = dt;
                Repeater2.DataBind();
                gridviewData.DataSource = dt;
                gridviewData.DataBind();
                fromDatePdf.Text = (FromDate.Text != "" ? "from: "+FromDate.Text : ""); 
                ToDatePdf.Text = ( ToDate.Text != "" ? ToDate.Text : DateTime.Now.ToString());
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void GetLastWeekSales_Click(object sender, EventArgs e)
    {
        String sDate = DateTime.Now.ToString();
        DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

        String day = datevalue.Day.ToString();
        String month = datevalue.Month.ToString();
        String year = datevalue.Year.ToString();
        string date = day + "-" + month + "-" + year;
        BindRepeator2(day,1);
    }
    protected void GetLastMonthSales_Click(object sender, EventArgs e)
    {
        String sDate = DateTime.Now.ToString();
        DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

        String day = datevalue.Day.ToString();
        String month = datevalue.Month.ToString();
        String year = datevalue.Year.ToString();
        string date = day + "-" + month + "-" + year;
        BindRepeator2(month, 2);
    }
    protected void AllSales_Click(object sender, EventArgs e)
    {
        BindRepeator2();
    }

    protected void GivenDateSearch_Click(object sender, EventArgs e)
    {
        BindRepeator2("a",3);
    }


    protected void PrintBtn_Click(object sender, EventArgs e)
    {
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", String.Format("attachment;filename=sales{0}to{1}.pdf",FromDate.Text, (ToDate.Text != "" ? ToDate.Text : DateTime.Now.ToString())));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        printPanel.RenderControl(hw);
        StringReader sr = new StringReader(sw.ToString());
        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 10f);
        HTMLWorker htmlParser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlParser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);

        Response.End();
    }
    protected void PrintBtn2_Click(object sender, EventArgs e)
    {
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", String.Format("attachment;filename=Total Sales.pdf"));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        printPanel2.RenderControl(hw);
        StringReader sr = new StringReader(sw.ToString());
        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 10f);
        HTMLWorker htmlParser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlParser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);

        Response.End();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }





}