﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        
        if (!this.IsPostBack)
        {
            string req_id = HttpUtility.UrlDecode(Request.QueryString["Id"]);
            //Account req_account = Account.authernicateById(Convert.ToInt32(req_id.ToString()));
            //Account curr_user = Account.authernicateById(Convert.ToInt32(Session["user_id"].ToString()));
            //if (curr_user.is_admin|| curr_user == req_account)
            //{
            //    NameTB.Text = req_account.username;
            //    EmailTB.Text = req_account.email;
            //    AddressTB.Text = req_account.address;
            //    MobileTB.Text = req_account.mobile_number;
            //    string is_admin = req_account.is_admin.ToString();
            //    AdminDDL.SelectedValue = req_account.is_admin.ToString();
            //    AdminLabelDiv.Visible = false;
            //    if (curr_user.is_admin) AdminLabelDiv.Visible = true;

            //}
            //else
            //{
                
            //        NameTB.Text = "لا يمكنك الوصول إلى الحساب المطلوب";
                
            //}



            string admin = HttpUtility.UrlDecode(Request.QueryString["is_admin"]);
            IdLabel.Text = req_id;
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
            {
                string query = "SELECT * FROM Accounts WHERE Id = @Id";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@id", req_id);
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmd;
                DataSet ds = new DataSet();
                sda.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    NameTB.Text = ds.Tables[0].Rows[0]["username"].ToString();
                    EmailTB.Text = ds.Tables[0].Rows[0]["email"].ToString();
                    AddressTB.Text = ds.Tables[0].Rows[0]["address"].ToString();
                    MobileTB.Text = ds.Tables[0].Rows[0]["mobile_number"].ToString();
                    string is_admin = ds.Tables[0].Rows[0]["is_admin"].ToString();
                    AdminDDL.SelectedValue = is_admin;
                    if (admin == "yes")
                    {
                        AdminLabelDiv.Visible = true;
                    }
                }
            }

        }
        
    }
    protected void UpdateAccount_Click(object sender, EventArgs e)
    {
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
            {
                bool number = IsDigitsOnly(MobileTB.Text);
                bool validator = true;
                string query = "UPDATE Accounts SET ";
                if (NameTB.Text.Length > 0)
                {
                    BoxValidator(NameTB, 2);
                    query += "username = @username,";
                }
                else
                {
                    BoxValidator(NameTB, 1);
                    validator = false;
                }
                if (EmailTB.Text.Length > 0 && EmailTB.Text.Contains("@") && EmailTB.Text.Contains("."))
                {
                    BoxValidator(EmailTB, 2);
                    query += "email = @email,";
                }
                else
                {
                    BoxValidator(EmailTB, 1);
                    validator = false;
                }
                if (PasswordTB.Text.Length > 0)
                {
                    query += "password = @password,";
                }
                if (AddressTB.Text.Length > 0)
                {
                    BoxValidator(AddressTB, 2);
                    query += "address = @address,";
                }
                else
                {
                    BoxValidator(AddressTB, 1);
                    validator = false;
                }
                if (MobileTB.Text.Length > 0 && number == true)
                {
                    BoxValidator(MobileTB, 2);
                    query += "mobile_number = @mobile_number";
                }
                else
                {
                    BoxValidator(MobileTB, 1);
                    validator = false;
                }

                if (validator == true)
                {
                    con.Open();
                    query += ", is_admin = @is_admin WHERE Id = @Id";
                    SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@Id", IdLabel.Text);
                    cmd.Parameters.AddWithValue("@username", NameTB.Text);
                    cmd.Parameters.AddWithValue("@email", EmailTB.Text);
                    if (PasswordTB.Text.Length > 0)
                    {
                        cmd.Parameters.AddWithValue("@password", PasswordTB.Text);

                    }
                    cmd.Parameters.AddWithValue("@address", AddressTB.Text);
                    cmd.Parameters.AddWithValue("@mobile_number", MobileTB.Text);
                    cmd.Parameters.AddWithValue("@is_admin", AdminDDL.SelectedValue);
                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.UpdateCommand = cmd;
                    sda.UpdateCommand.ExecuteNonQuery();
                    testlabel.Text = "تم تعديل المعلومات";
                    
                    
                }
                else
                {
                    testlabel.Text = "الرجاء التاكد من المعلومات المطلوبة.";
                }

            }

    }

    protected void BoxValidator(TextBox textboxName, int num)
    {
        if (num == 1)
        {
            textboxName.CssClass = "input danger";
        }
        else
        {
            textboxName.CssClass = "input";
        }

    }
    bool IsDigitsOnly(string str)
    {
        foreach (char c in str)
        {
            if (c < '0' || c > '9')
                return false;
        }

        return true;
    }
}