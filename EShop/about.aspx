﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="about.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>من نحن&nbsp;|&nbsp;EShop</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <div class="main inside-main">
        <div>
            <div class="float-right-txt">
                <p>
                    .تأسست الشركة عام 2019 بهدف تقديم تجربة جديدة للمستخدمين من خلال التسوق من متجر إلكتروني متخصّص ببيع الأجهزة الإلكترونيّة (حواسيب، شاشات، طابعات...)
                </p>
                <p style="text-align: right;">
                    .نهدف إلى إرضاء الزبائن وتقديم أفضل الخدمات، مع إمكانية الدفع عبر المصارف المتوفرة أو نقداً
                </p>
            </div>

            <div class="float-right-txt" style="height: auto">
                <h2>المُطوِّرون
                </h2>
                (omar_108591) عمر نوفل   
                        <br />
                (omar_116205) عمر غوكا 
                        <br />
                (mhd_hussam_109817) محمد حسام حباس
                        <br />
                <br />
            </div>
            <div class="float-right-txt text-center" style="height: auto; text-align:center!important;">
                <iframe width="560" height="315"  src="https://www.youtube.com/embed/2gYpiod_wo4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


            </div>
        </div>
        <br />
    </div>


</asp:Content>

