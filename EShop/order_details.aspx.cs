﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Text.RegularExpressions;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

public partial class _Default : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["username"] == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
            if (!this.IsPostBack)
            {

                BindRepeator();
                
            }
        }
        
    }
    private void BindRepeator()
    {
        int total_price = 0;
        string orderid = HttpUtility.UrlDecode(Request.QueryString["Id"]);
        using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))
        {
            string query = "SELECT * FROM Order_Placed WHERE order_id = '" + orderid + "'";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            con.Open();
            DataTable dt = new DataTable();
            sda.Fill(dt);
            Repeater1.DataSource = dt;
            
            gridviewData.DataSource = dt;
            Repeater1.DataBind();
            
            string[] delete = { "product_image"};
            foreach (DataRow row in dt.Rows)
            {
                int price = Convert.ToInt32(Regex.Match(row["total_price"].ToString(), @"\d+").Value);
                total_price += price;
            }
            foreach (string ColName in delete)
            {
                if (dt.Columns.Contains(ColName))
                    dt.Columns.Remove(ColName);
                
            }
            gridviewData.DataBind();
            total.Text = total_price.ToString();
        }
    }
    protected void PrintBtn_Click(object sender, EventArgs e)
    {
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition",String.Format("attachment;filename=bill_{0}.pdf", HttpUtility.UrlDecode(Request.QueryString["Id"])));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        printPanel.RenderControl(hw);
        StringReader sr = new StringReader(sw.ToString());
        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 10f);
        HTMLWorker htmlParser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlParser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        
        Response.End();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
}