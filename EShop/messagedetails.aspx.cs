﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class messagedetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Convert.ToString(Session["username"]) != "")
        {
            string cond = String.Format("id={0}", Convert.ToString(Session["user_id"]));
            Account cur_user = Account.filter(cond)[0];
            if (cur_user.is_admin)
            {
                idlbl.Text = HttpUtility.UrlDecode(Request.QueryString["Id"]);
                using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["EShopDBConnectionString"].ConnectionString))

                {
                    cond = String.Format("WHERE id= {0}", HttpUtility.UrlDecode(Request.QueryString["Id"]));
                    messages mess = messages.filter(cond)[0];
                    con.Open();
                    string query2 = "SELECT * from messages where id = @id";
                    SqlCommand cmd2 = new SqlCommand(query2, con);
                    cmd2.Parameters.AddWithValue("@id", HttpUtility.UrlDecode(Request.QueryString["Id"]));
                    SqlDataAdapter sda2 = new SqlDataAdapter();
                    sda2.SelectCommand = cmd2;
                    DataSet ds2 = new DataSet();
                    sda2.Fill(ds2);
                    con.Close();
                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        namelbl.Text = mess.name;
                        emaillbl.Text = ds2.Tables[0].Rows[0]["email"].ToString();
                        contentlbl.Text = ds2.Tables[0].Rows[0]["content"].ToString();
                        datelbl.Text = ds2.Tables[0].Rows[0]["date"].ToString();
                        isreadlbl.Text = ds2.Tables[0].Rows[0]["is_read"].ToString();
                    }
                    else
                    {
                        idlbl.Text = "404ERORR";
                    }



                }

            }
            else
            {
                idlbl.Text = "admin only";
            }


        }
        else
        {
            idlbl.Text = "login and try agine";
        }
    }

}