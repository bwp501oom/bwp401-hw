﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="shopping_cart.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>سلة الشراء&nbsp;|&nbsp;EShop</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <div class="main inside-main" style="direction: rtl;">
        <br />
        <br />
        <div class="row text-center" id="NoProductsFound" runat="server" visible="false">
            <div class="col-md-12">
                <asp:Label runat="server" ID="lbl1" Text=""></asp:Label>
            </div>

        </div>
        <div class="row text-center" style="margin: auto; background-color: #232F3E; color: white; font-size: 24px;" runat="server" id="TopHeader">
            <div class="col-md-5">
                <asp:Label runat="server" Text="صورة المنتج"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="اسم المنتج"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="الكمية"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:Label runat="server" Text="السعر الاجمالي"></asp:Label>
            </div>
            <div class="col-md-1" style="margin: auto;">
                <asp:Label runat="server" Text="حذف"></asp:Label>
            </div>
        </div>
        <asp:Repeater runat="server" ID="Repeater1">
            <ItemTemplate>
                <div class="row  text-center" style="margin: auto;">
                    <div class="col-md-5">
                        <img class="text-right" src='<%#Eval("product_image")%>' alt="Card image cap" style="width: 40%;">
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("product_name")%>'></asp:Label>
                        <asp:Label runat="server" Text='<%#Eval("Id")%>' Visible="false" ID="ProductId"></asp:Label>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("quantity")%>'></asp:Label>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("total_price")%>'></asp:Label>

                    </div>
                    <div class="col-md-1" style="margin: auto;">
                        <asp:LinkButton runat="server" ID="DeleteProduct" Text="حذف" OnClientClick="return confirm('هل انت متأكد من حذف هذا المنتج من سلة الشراء؟');" OnClick="DeleteProduct_Click" />
                    </div>
                </div>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <div class="row  text-center" style="margin: auto; background-color: #EFF0F1;">
                    <div class="col-md-5">
                        <img class="text-right" src='<%#Eval("product_image")%>' alt="Card image cap" style="width: 40%;">
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("product_name")%>'></asp:Label>
                        <asp:Label runat="server" Text='<%#Eval("Id")%>' Visible="false" ID="ProductId"></asp:Label>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("quantity")%>'></asp:Label>
                    </div>
                    <div class="col-md-2" style="margin: auto;">
                        <asp:Label runat="server" Text='<%#Eval("total_price")%>'></asp:Label>
                    </div>
                    <div class="col-md-1" style="margin: auto;">
                        <asp:LinkButton runat="server" ID="DeleteProduct" Text="حذف" OnClientClick="return confirm('هل انت متأكد من حذف هذا المنتج من سلة الشراء؟');" OnClick="DeleteProduct_Click" />
                    </div>
                </div>
            </AlternatingItemTemplate>
        </asp:Repeater>
        <br />
        <br />
        <div class="row text-center" runat="server" id="TotalPriceDivOrder">
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <label>مجموع سعر المنتجات :</label>
                <asp:Label runat="server" ID="TotalPrice" Text=""></asp:Label>
                <asp:Label runat="server" ID="ItemCount" Text="" Visible="false"></asp:Label>
            </div>
            <div class="col-md-4">
                <asp:Button runat="server" ID="SubmitOrder" Text="ادفع" OnClick="SubmitOrder_Click" />
                <asp:Label runat="server" ID="MaxCounter" Text="" Visible="false"></asp:Label>
                <asp:Label runat="server" ID="ProductHolderLabel" Text="" Visible="false"></asp:Label>

            </div>
            <asp:Label runat="server" ID="testlabel" Text=""></asp:Label>
            <div class="" style="direction:ltr;">
                <asp:Label runat="server" ID="testlabel2" Text=""></asp:Label>
                <br />
                <asp:Label runat="server" ID="testlabel3" Text=""></asp:Label>
            </div>
            
        </div>
           <div runat="server" id ="PastOrdersDiv">
                <div class="row">
                <div class="col-md-12 text-center" style="font-size: 30px;">
                    <label>طلباتي السابقة</label>
                </div>
            </div>
            <br />
            <div class="row text-center" style="margin: auto; background-color: #232F3E; color: white; font-size: 24px;">
                <div class="col-md-2">
                    <label>رقم الطلب</label>
                </div>
                <div class="col-md-5">
                    <label>السعر الاجمالي</label>
                </div>
                <div class="col-md-2">
                    <label>عنوان التوصيل</label>
                </div>
                <div class="col-md-1">
                    <label>الحالة</label>
                </div>
                <div class="col-md-1">
                    <label>التفاصيل</label>
                </div>
                <div class="col-md-1">
                    <label>حذف</label>
                </div>
            </div>
            <asp:Repeater runat="server" ID="Repeater2">
                <ItemTemplate>
                    <div class="row  text-center" style="margin: auto;">
                            <div class="col-md-2">
                                <asp:Label runat="server" Text='<%#Eval("order_id")%>' ID="OrderID"></asp:Label>
                                <asp:Label runat="server" Text='<%#Eval("Id")%>' Visible="false"></asp:Label>
                            </div>
                            <div class="col-md-5" style="margin: auto;">
                                <asp:Label runat="server" Text='<%#Eval("order_price")%>'></asp:Label>
                            </div>
                            <div class="col-md-2" style="margin: auto;">
                                <asp:Label runat="server" Text='<%#Eval("order_address")%>'></asp:Label>
                            </div>
                            <div class="col-md-1" style="margin: auto;">
                                <asp:Label runat="server" Text='<%#Eval("status")%>'></asp:Label>
                            </div>
                        <div class="col-md-1" style="margin: auto;">
                                <a runat="server" href='<%# Eval("order_id", "~/order_details.aspx?Id={0}") %>'>التفاصيل</a>
                            </div>
                        <div class="col-md-1" style="margin: auto;">
                            <asp:LinkButton runat="server" ID="DeleteOrder" Text="حذف" OnClientClick="return confirm('هل انت متأكد من حذف هذا المنتج من سلة الشراء؟');" OnClick="DeleteOrder_Click" />
                        </div>
                    </div>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <div class="row  text-center" style="margin: auto; background-color: #EFF0F1;">
                        
                            <div class="col-md-2">
                                <asp:Label runat="server" Text='<%#Eval("order_id")%>' ID="OrderID"></asp:Label>
                                <asp:Label runat="server" Text='<%#Eval("Id")%>' Visible="false"></asp:Label>
                            </div>
                            <div class="col-md-5" style="margin: auto;">
                                <asp:Label runat="server" Text='<%#Eval("order_price")%>'></asp:Label>
                            </div>
                            <div class="col-md-2" style="margin: auto;">
                                <asp:Label runat="server" Text='<%#Eval("order_address")%>'></asp:Label>
                            </div>
                            <div class="col-md-1" style="margin: auto;">
                                <asp:Label runat="server" Text='<%#Eval("status")%>'></asp:Label>
                            </div>
                              <div class="col-md-1" style="margin: auto;">
                                <a runat="server" href='<%# Eval("order_id", "~/order_details.aspx?Id={0}") %>'>التفاصيل</a>
                            </div>
                                
                        <div class="col-md-1" style="margin: auto;">
                            <asp:LinkButton runat="server" ID="DeleteOrder" Text="حذف" OnClientClick="return confirm('هل انت متأكد من حذف هذا المنتج من سلة الشراء؟');" OnClick="DeleteOrder_Click" />
                        </div>
                    </div>
                </AlternatingItemTemplate>
            </asp:Repeater>
           </div>




        <br />
        <br />
        <br />
    </div>
</asp:Content>

