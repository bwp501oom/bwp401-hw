﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="products.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>المنتجات&nbsp;|&nbsp;EShop</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <div class="main inside-main" style="direction: rtl;">
        <div class="row" runat="server" id="AddProductDiv" visible="false">
            <div class="col-md-12 text-center">
                <asp:Button runat="server" ID="NewProduct" Visible="true" Text="منتج جديد" CssClass="submit-btn" OnClick="NewProduct_Click" />
            </div>
        </div>
        <br />
        <br />
        <div class="row text-right" style="padding-right: 30px; margin: auto 0px;">
            <div class="col-md-6">
                <label>بحث عام :</label>
                <asp:TextBox runat="server" ID="SearchBox" CssClass="box" OnTextChanged="SearchBox_TextChanged" AutoPostBack="true"></asp:TextBox>
            </div>
            <div class="col-md-6">
                <label>اضافة فلتر :</label>
                <asp:DropDownList runat="server" ID="FilterDDL" CssClass="box" OnSelectedIndexChanged="FilterDDL_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Enabled="true" Text="اختر نوع الفلتر"></asp:ListItem>
                    <asp:ListItem Text="اسم المنتج" Value="productName"></asp:ListItem>
                    <asp:ListItem Text="سعر المنتج" Value="productPrice"></asp:ListItem>
                    <asp:ListItem Text="سنة الانتاج" Value="productYear"></asp:ListItem>
                    <asp:ListItem Text="نوع المنتج" Value="productType"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <br />
        <div class="row text-right" style="padding-right: 30px; margin: auto 0px;">
            <div class="col-md-6" runat="server" id="ProductNameDiv" visible="false">
                <div class="row">
                    <div class="col-md-9">
                        <label>اسم المنتج :</label>
                        <asp:TextBox runat="server" ID="ProductNameTB" Text="" CssClass="box"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:Button runat="server" ID="DeleteProductNameTB" Text="حذف" CssClass="danger-btn-edited" placeholder="اسم المنتج" OnClick="DeleteProductNameTB_Click"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6"  runat="server" id="ProductPriceDiv"  visible="false">
                <label>سعر المنتج :</label>
                <asp:TextBox runat="server" ID="ProductPriceMinTB" Text="" CssClass="box-edited" placeholder="أقل سعر"></asp:TextBox>
                <asp:TextBox runat="server" ID="ProductPriceMaxTB" Text="" CssClass="box-edited" placeholder="أغلى سعر"></asp:TextBox>
                <asp:Button runat="server" ID="DeleteProductPriceMinTB" Text="حذف" CssClass="danger-btn-edited" OnClick="DeleteProductPriceMinTB_Click"/>
            </div>
            <div class="col-md-6" runat="server" id="ProductTypeDiv" visible="false">
                <div class="row">
                    <div class="col-md-9" runat="server">
                        <label>نـوع المنتج :</label>
                        <asp:DropDownList runat="server" ID="ProductTypeDDL" CssClass="box">
                            <asp:ListItem Enabled="true" Text="الجميع" Value=""></asp:ListItem>
                            <asp:ListItem Text="حاسب" Value="pc"></asp:ListItem>
                            <asp:ListItem Text="لابتوب" Value="laptop"></asp:ListItem>
                            <asp:ListItem Text="طابعة" Value="printer"></asp:ListItem>
                            <asp:ListItem Text="ماسح ضوئي" Value="scanner"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <asp:Button runat="server" ID="DeleteProductTypeDDL" Text="حذف" CssClass="danger-btn-edited" OnClick="DeleteProductTypeDDL_Click"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6" runat="server" id="ProductYearDiv" visible="false">
                <label>سنة الانتـاج :</label>
                <asp:TextBox runat="server" ID="ProductYearMinTB" Text="" CssClass="box-edited" placeholder="من السنة"></asp:TextBox>
                <asp:TextBox runat="server" ID="ProductYearMaxTB" Text="" CssClass="box-edited" placeholder="الى السنة"></asp:TextBox>
                <asp:Button runat="server" ID="DeleteYearDivBtn" Text="حذف" CssClass="danger-btn-edited" OnClick="DeleteYearDivBtn_Click"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <asp:Button runat="server" ID="SearchBtn" CssClass="submit-btn" Text="ابحث" OnClick="SearchBtn_Click"/>
            </div>
            <div class="col-md-12 text-center">
                <asp:Label runat="server" ID="FilterValidator" Text=""></asp:Label>
                
            </div>
        </div>
        <br />
        <div class="row card-deck">
            <asp:Repeater runat="server" ID="Repeater1">
                <ItemTemplate>
                    <div class="col-md-4" style="padding-top: 10px;">
                        <a runat="server" href='<%# Eval("id", "~/product_details.aspx?Id={0}") %>'>
                            <img class="card-img-top" src='<%#Eval("image")%>' alt="Card image cap" style="height: 50%; width: 50%;">
                            <div class="card-body">
                                <asp:Label runat="server" Text='<%#Eval("product_name")%>' CssClass="card-title"></asp:Label>
                            </div>
                            <div class="card-footer">
                                <label>SYR</label>
                                <asp:Label runat="server" Text='<%#Eval("price")%>'></asp:Label>
                            </div>
                        </a>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <br />
        <br />
        <div class="row">
            <div class="col-md-12 text-center">
                <asp:Label runat="server" ID="validator" Text=""></asp:Label>
            </div>
        </div>
        <br />
    </div>
</asp:Content>

